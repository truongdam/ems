package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/14/2017.
 */

public class TokenDTO {

    private String json_token;

    public TokenDTO(String json_token) {
        this.json_token = json_token;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }
}
