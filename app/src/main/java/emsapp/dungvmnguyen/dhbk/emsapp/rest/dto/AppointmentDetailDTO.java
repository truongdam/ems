package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/23/2017.
 */

public class AppointmentDetailDTO {

    private String json_token;
    private Integer appointment_id;
    private Integer vehicle_id;
    private String start_time;
    private String start_location;


    public AppointmentDetailDTO(String json_token, Integer appointment_id, Integer vehicle_id, String start_time, String start_location) {
        this.json_token = json_token;
        this.appointment_id = appointment_id;
        this.vehicle_id = vehicle_id;
        this.start_time = start_time;
        this.start_location = start_location;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }

    public Integer getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(Integer appointment_id) {
        this.appointment_id = appointment_id;
    }

    public Integer getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(Integer vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStart_location() {
        return start_location;
    }

    public void setStart_location(String start_location) {
        this.start_location = start_location;
    }
}
