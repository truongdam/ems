package emsapp.dungvmnguyen.dhbk.emsapp.rest.service;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by truong d on 10/21/2017.
 */

public interface HistoryService {
    @POST("/api/getCompletedAppointments  ")
    Call<HashMap> getAllHistoryAppointment(@Body TokenDTO postToken);
}
