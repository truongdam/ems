package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.Appointment;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.AppointmentAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AppointmentFragment extends Fragment {

    ListView lvAppointment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_appointment, container, false);

        Call<HashMap> getAllAppointments = RestClient.getInstance().getAppointmentService().getAllAppointment(new TokenDTO(Global.getToken()));
        getAllAppointments.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap activeAppointmentsObject = response.body();
                ArrayList listActiveAppointments = (ArrayList) activeAppointmentsObject.get("listActiveAppointments");
                Integer message = (Integer) activeAppointmentsObject.get("message");

                if (message == 1 ) {

                    final ArrayList<Appointment> listAppointment = new ArrayList<>();
                    lvAppointment = (ListView) view.findViewById(R.id.lvAppointment);

                    try {
                        Iterator itr = listActiveAppointments.iterator();
                        while(itr.hasNext()) {
                            HashMap activeAppointment = (HashMap) itr.next();
                            HashMap client = (HashMap) activeAppointment.get("client");
                            Appointment a = new Appointment((String) activeAppointment.get("name"), (String) activeAppointment.get("destination"), (String) activeAppointment.get("start_date"), (String) client.get("name"));
                            a.setId((Integer) activeAppointment.get("id"));
                            listAppointment.add(a);
                        }
                        AppointmentAdapter adapter = new AppointmentAdapter(
                                getContext(),
                                R.layout.apponitment_row,
                                listAppointment
                        );
                        lvAppointment.setAdapter(adapter);
                        lvAppointment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Global.setAppointmentId(listAppointment.get(position).getId());
                                showFragment();
                            }
                        });
                    }catch (Exception e){

                    }
                }
                else {
                    Toast.makeText(getContext(), "Get list active appointments unsuccessful.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {

            }
        });

        return view;
    }

    private void showFragment() {
        ActivationFragment newFragment = new ActivationFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}