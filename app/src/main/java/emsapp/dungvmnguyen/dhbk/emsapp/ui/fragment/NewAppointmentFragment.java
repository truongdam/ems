package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;


import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAppointmentFragment extends Fragment {

    EditText edtName, edtDes, edtDate;
    Button btnSave, btnBack;
    ImageView img_add_emp;
    TextView txtAddEmp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_appointment, container, false);
        Bundle bundle = this.getArguments();

        edtName = (EditText) view.findViewById(R.id.edtCustomerName);
        edtDes = (EditText) view.findViewById(R.id.edtLocation);

        edtDate = (EditText) view.findViewById(R.id.edtDate);
        edtDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                chooseDate();
            }
        });

        img_add_emp = (ImageView) view.findViewById(R.id.img_add_emp);
        img_add_emp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListUserFragment listUserFragment = new ListUserFragment();
                showFragment(listUserFragment);
            }
        });

        txtAddEmp = (TextView) view.findViewById(R.id.txtAddEmp);

        if (Global.getUserIds().size() == 1) {
            txtAddEmp.setText("There is one chosen employee in list");
        } else if (Global.getUserIds().size() > 1) {
            String temp = "There are " + Global.getUserIds().size() + " chosen employees in list";
            txtAddEmp.setText(temp);
        } else {
            txtAddEmp.setText("Add employee");
        }

        if (bundle != null) {
            edtName.setText(bundle.getString("customName"));
            edtDes.setText(bundle.getString("destination"));
            edtDate.setText(bundle.getString("startDate"));
        }

        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<HashMap> createAppointment = RestClient.getInstance().getAppointmentService().createAppointment(new AppointmentDTO(Global.getToken(), edtName.getText().toString(), 1, edtDes.getText().toString(), edtDate.getText().toString(), Global.getUserIds()));
                createAppointment.enqueue(new Callback<HashMap>() {
                    @Override
                    public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                        HashMap createNew = response.body();
                        Integer message = (Integer) createNew.get("message");

                        if (message == 1) {
                            Toast.makeText(getContext(), "Create successed", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getContext(), "Create failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<HashMap> call, Throwable t) {
                        Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void chooseDate(){
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                calendar.set(y, m ,d);
                TimeZone tz1 = TimeZone.getTimeZone("GMT+7");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                simpleDateFormat.setTimeZone(tz1);
                edtDate.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void showFragment(Fragment newFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundleobj = new Bundle();
        bundleobj.putCharSequence("customName", edtName.getText().toString());
        bundleobj.putCharSequence("destination", edtDes.getText().toString());
        bundleobj.putCharSequence("startDate", edtDate.getText().toString());
        newFragment.setArguments(bundleobj);
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
