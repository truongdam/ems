package emsapp.dungvmnguyen.dhbk.emsapp.rest.service;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AuthenticationDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.PasswordDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    @POST("/api/login")
    Call<HashMap> login(@Body AuthenticationDTO authenticationDTO);

    @POST("/api/manager/getEmployees")
    Call<HashMap> getManagedUsersByOneManager(@Body TokenDTO tokenDTO);

    @POST ("api/changepass")
    Call<HashMap> changePassword(@Body PasswordDTO json);

    @POST ("api/getInfo")
    Call<HashMap> getProfile (@Body TokenDTO tokenDTO);
}
