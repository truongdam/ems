package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.User;

/**
 * Created by WINDOWS on 10/15/2017.
 */

public class UserAdapter extends BaseAdapter {

    Activity activity;
    List<User> users;
    LayoutInflater inflater;

    public UserAdapter(Activity activity) {
        this.activity = activity;
    }

    public UserAdapter(Activity activity, List<User> users) {
        this.activity = activity;
        this.users = users;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.user_row, parent, false);

            holder = new ViewHolder();

            holder.fullName = (TextView)convertView.findViewById(R.id.txtFullName);
            holder.email = (TextView)convertView.findViewById(R.id.txtEmail);
            holder.type = (TextView)convertView.findViewById(R.id.txtType);
            holder.ivCheckBox = (ImageView)convertView.findViewById(R.id.ivCheckBox);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        User user = users.get(position);

        holder.fullName.setText(user.getFullname());
        holder.email.setText(user.getEmail());
        holder.type.setText(user.getType());

        if (user.isActive())
            holder.ivCheckBox.setBackgroundResource(R.drawable.ic_checked);
        else
            holder.ivCheckBox.setBackgroundResource(R.drawable.ic_uncheck);

        return convertView;
    }

    public void updateRecords(List<User> users) {
        this.users = users;

        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView fullName;
        TextView email;
        TextView type;
        ImageView ivCheckBox;
    }
}
