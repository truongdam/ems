package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/7/2017.
 */

public class Appointment {

    private Integer id;
    private String name;
    private String destination;
    private String start_date;
    private String client_name;

    public Appointment(String name, String destination, String start_date, String client_name) {
        this.name = name;
        this.destination = destination;
        this.start_date = start_date;
        this.client_name = client_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }
}
