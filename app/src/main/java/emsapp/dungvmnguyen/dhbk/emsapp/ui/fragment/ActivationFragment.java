package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentCompleteDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDetailDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.CoordinateDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.CoordinateDTO2;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.DetailDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.service.GPS_Service;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.activity.AppointmentActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationFragment extends Fragment  {
    MapView mMapView;

    private Integer vehicleId;
    private GoogleMap mMap;
    private Button btnStart, btnStop, btnChooseVehicle, btnComplete;
    private TextView txtChosenVehicle;
    private BroadcastReceiver broadcastReceiver;
    private ArrayList<CoordinateDTO> arrayCoordinate = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activation, container, false);
        btnStart = (Button) view.findViewById(R.id.btnStart);
        btnStop = (Button) view.findViewById(R.id.btnFinish);
        btnComplete = (Button) view.findViewById(R.id.btnComplete);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        txtChosenVehicle = (TextView) view.findViewById(R.id.txtChosenVehicle);

        btnChooseVehicle = (Button) view.findViewById(R.id.btnChooseVehicle);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            vehicleId = Integer.parseInt(bundle.getString("vehicleId"));
            txtChosenVehicle.setText(String.format("%s is being chosen", bundle.getString("vehicleName")));
            btnStart.setEnabled(true);
            btnStop.setEnabled(false);
        }
        else {
            btnStart.setEnabled(false);
            btnStop.setEnabled(false);
            txtChosenVehicle.setText("No vehicle is being chosen");
        }

        btnComplete.setEnabled(false);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
            }
        });
        if (!runtime_permissions())
            enable_buttons();
        return view;
    }

    @Override
    public void onDestroy() {
        if (broadcastReceiver != null){
            getContext().unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();

    }

    public void DrawLine(CoordinateDTO coordinateStart, CoordinateDTO coordinateEnd) {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions
                .add(new LatLng(coordinateStart.getpLat(), coordinateStart.getpLong()))
                .add(new LatLng(coordinateEnd.getpLat(), coordinateEnd.getpLong()))
                .color(Color.BLUE)
                .endCap(new RoundCap())
                .jointType(JointType.ROUND);

        mMap.addPolyline(polylineOptions);

    }

    private  void DrawMarker (CoordinateDTO startPoint){
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(startPoint.getpLat(), startPoint.getpLong()))
                .title("Start"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(startPoint.getpLat(), startPoint.getpLong()), 15));
    }

    @Override
    public void onResume() {

        if (Global.getCheckExistCost()){
            btnComplete.setEnabled(true);
        }
        else
        {
            btnComplete.setEnabled(false);
        }
        if (broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {


                    if(arrayCoordinate.size() <1){
                        arrayCoordinate.add(
                                new CoordinateDTO(Double.parseDouble(intent.getExtras().get("Long")+""),
                                        Double.parseDouble(intent.getExtras().get("Lat")+""),intent.getExtras().getString("time")));
                        DrawMarker(arrayCoordinate.get(0));
                    }
                    else {
                        CoordinateDTO lastCoordinate = arrayCoordinate.get(arrayCoordinate.size() - 1);
                        float[] results = new float[100];
                        Location.distanceBetween(lastCoordinate.getpLat(), lastCoordinate.getpLong(),  Double.parseDouble(intent.getExtras().get("Lat")+""), Double.parseDouble(intent.getExtras().get("Long")+""), results);

                        if (results[0] >= 3.0){
                            arrayCoordinate.add(
                                    new CoordinateDTO(Double.parseDouble(intent.getExtras().get("Long")+""),
                                            Double.parseDouble(intent.getExtras().get("Lat")+""),intent.getExtras().getString("time")));

                            CoordinateDTO start = arrayCoordinate.get(arrayCoordinate.size() - 2);
                            CoordinateDTO end = arrayCoordinate.get(arrayCoordinate.size() - 1);
                            CameraUpdateFactory.newLatLng(new LatLng(end.getpLat(), end.getpLat()));
                            DrawLine(start, end);
                        }
                        else Toast.makeText(getContext(),Float.toString(results[0]), Toast.LENGTH_SHORT).show();
                    }
                }
            };
        }
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
        mMapView.onResume();
        super.onResume();

    }

    private void enable_buttons(){

        btnChooseVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListVehicleFragment listVehicleFragment = new ListVehicleFragment();
                showFragment(listVehicleFragment);
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateFormat formatter = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
                String date = (String) formatter.format(new Date(System.currentTimeMillis()));
                Call<HashMap> createDetail = RestClient.getInstance().getAppointmentService().createDetail(new AppointmentDetailDTO(Global.getToken(), Global.getAppointmentId(), vehicleId, date, ""));
                createDetail.enqueue(new Callback<HashMap>() {
                    @Override
                    public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                        HashMap res = response.body();
                        Global.setDetail_id((Integer) res.get("detail_id"));
                        if ((Integer) res.get("message") == 1) {
                            btnStart.setEnabled(false);
                            btnStop.setEnabled(true);
                            btnComplete.setEnabled(false);
                            btnChooseVehicle.setEnabled(false);
                            Toast.makeText(getContext(), "Current vehicle started successfully", Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(getContext(), "Current vehicle did'nt start", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<HashMap> call, Throwable t) {
                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
                Intent i = new Intent(getActivity(), GPS_Service.class);
                getActivity().startService(i);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), GPS_Service.class);
                getActivity().stopService(i);

                List<CoordinateDTO2> coordinates = new ArrayList<CoordinateDTO2>();
                Iterator itr = arrayCoordinate.iterator();
                while (itr.hasNext()) {
                    CoordinateDTO coordinateDTO = (CoordinateDTO) itr.next();
                    coordinates.add(new CoordinateDTO2(coordinateDTO.getTime(), coordinateDTO.getpLat(), coordinateDTO.getpLong()));
                }
                coordinates.add(new CoordinateDTO2("11:55:01 14-05-2017", 109.9, 123.3));
                coordinates.add(new CoordinateDTO2("12:58:01 14-05-2017", 119.9, 143.3));
                Call<HashMap> addCoordinate = RestClient.getInstance().getAppointmentService().addCoordinate2(new DetailDTO(Global.getToken(),Global.getDetail_id(),coordinates));
                addCoordinate.enqueue(new Callback<HashMap>() {
                    @Override
                    public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                        HashMap res = response.body();
                        if ((Integer) res.get("message") == 1) {
                            btnStart.setEnabled(false);
                            btnStop.setEnabled(false);
                            btnChooseVehicle.setEnabled(true);
                            btnComplete.setEnabled(true);
                            Toast.makeText(getContext(), "Coordinates were sent successfully ", Toast.LENGTH_SHORT).show();
                            InputCostFragment input_dialog = new InputCostFragment();
                            showFragment(input_dialog);
                        }
                        else
                            Toast.makeText(getContext(), "Coordinate were'nt sent", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<HashMap> call, Throwable t) {
                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<HashMap> endAppointment = RestClient.getInstance().getAppointmentService().endAppointment(new AppointmentCompleteDTO(Global.getAppointmentId(), "01:15 15-05-2017", Global.getToken(), 300000.0));
                endAppointment.enqueue(new Callback<HashMap>() {
                    @Override
                    public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                        HashMap res = response.body();
                        JSONObject json = new JSONObject(res);

                        if (json.has("message")){
                            try {
                                if (json.get("message").equals(1)){
                                    btnStart.setEnabled(false);
                                    btnStop.setEnabled(false);
                                    btnChooseVehicle.setEnabled(false);
                                    btnComplete.setEnabled(false);
                                    Toast.makeText(getContext(), "Appointment finished successfully", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(getContext(), "Appointment finished fail", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        Global.setTotalCost(0.0);
                        Global.setCheckExistCost(false);

                        showActivity(AppointmentActivity.class);
                    }
                    @Override
                    public void onFailure(Call<HashMap> call, Throwable t) {
                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private boolean runtime_permissions(){
        if(Build.VERSION.SDK_INT >=23 && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED){

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION},100);
            return true;
        }
        return false;
    };

    private void showFragment(Fragment newFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showActivity(Class activity) {
        Intent intent = new Intent(getContext(), activity);
        startActivity(intent);
    }
}
