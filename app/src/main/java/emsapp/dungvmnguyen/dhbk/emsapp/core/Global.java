package emsapp.dungvmnguyen.dhbk.emsapp.core;

import android.app.Application;

import java.util.ArrayList;

/**
 * Created by WINDOWS on 9/30/2017.
 */

public class Global extends Application {

    private static String token;
    private static String type;
    private static Integer employeeId;
    private static Integer appointmentId;
    private static ArrayList<Integer> userIds;
    private static Integer InputCost;
    private static Integer detail_id;
    private static Boolean checkExistCost = false;
    private static Double totalCost;
    private static String empty_img;

    public static void setToken(String token) {
        Global.token = token;
    }

    public static void setType(String type) {
        Global.type = type;
    }

    public static String getToken() {
        return token;
    }

    public static String getType() {
        return type;
    }

    public static Integer getAppointmentId() {
        return appointmentId;
    }

    public static void setAppointmentId(Integer appointmentId) {
        Global.appointmentId = appointmentId;
    }

    public static Integer getEmployeeId() {
        return employeeId;
    }

    public static void setEmployeeId(Integer employeeId) {
        Global.employeeId = employeeId;
    }

    public static ArrayList<Integer> getUserIds() {
        return userIds;
    }

    public static void setUserIds(ArrayList<Integer> userIds) {
        Global.userIds = userIds;
    }

    public static Integer getInputCost() {
        return InputCost;
    }

    public static void setInputCost(Integer inputCost) {
        InputCost = inputCost;
    }

    public static Integer getDetail_id() {
        return detail_id;
    }

    public static void setDetail_id(Integer detail_id) {
        Global.detail_id = detail_id;
    }

    public static Boolean getCheckExistCost() {
        return checkExistCost;
    }

    public static void setCheckExistCost(Boolean checkExistCost) {
        Global.checkExistCost = checkExistCost;
    }

    public static Double getTotalCost() {
        return totalCost;
    }

    public static void setTotalCost(Double totalCost) {
        Global.totalCost = totalCost;
    }

    public static String getEmpty_img() {
        return empty_img;
    }

    public static void setEmpty_img(String empty_img) {
        Global.empty_img = "iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAAAXNSR0IArs4c6QAAAARnQU1BAACx\n" +
                "jwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVh\n" +
                "ZHlxyWU8AAADeGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78i\n" +
                "IGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9i\n" +
                "ZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwNjcgNzkuMTU3NzQ3LCAy\n" +
                "MDE1LzAzLzMwLTIzOjQwOjQyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93\n" +
                "d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJk\n" +
                "ZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIg\n" +
                "eG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJl\n" +
                "ZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2lu\n" +
                "YWxEb2N1bWVudElEPSJ4bXAuZGlkOjk0NDYxOTY0LWIxMjMtNGRlNC1hNDNjLTc3MTZhMTI4YmZk\n" +
                "MSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpBRTYzMDJGQTU0NDExMUU1OEQzQThCMEM4QTQ3\n" +
                "RjdFRiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpBRTYzMDJGOTU0NDExMUU1OEQzQThCMEM4\n" +
                "QTQ3RjdFRiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoTWFjaW50\n" +
                "b3NoKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjk0NDYx\n" +
                "OTY0LWIxMjMtNGRlNC1hNDNjLTc3MTZhMTI4YmZkMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRp\n" +
                "ZDo5NDQ2MTk2NC1iMTIzLTRkZTQtYTQzYy03NzE2YTEyOGJmZDEiLz4gPC9yZGY6RGVzY3JpcHRp\n" +
                "b24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/4JNdAABK60lE\n" +
                "QVR4Xu3dd7Bc5Xn48ZciUEMUgSSaECAJSRQhIZoAUw0GTHGcxJBiJzOJE4/jZPxH/vGkeTLJTJLJ\n" +
                "+JcynkxsT+w4xjY22Nj0YoppAiSQLLqQQKiBUEENgQD/9H113sth2Xvv7r17d8/q/X5mztzds+WU\n" +
                "Pfd9nrecc/b49U5BkiRlZc/iryRJyogJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJk\n" +
                "AiBJUoZMACRJypAJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJUoZMACRJypAJ\n" +
                "gCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJUoZMACRJypAJgCRJGTIBkCQpQyYA\n" +
                "kiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJUoZMACRJypAJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBI\n" +
                "kpQhEwBJkjJkAiBJUoZMACRJypAJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJ\n" +
                "UoZMACRJypAJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJUoZMACRJypAJgCRJ\n" +
                "GTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJUoZMACRJypAJgCRJGTIBkCQpQyYAkiRl\n" +
                "yARAkqQMmQBIkpQhEwBJkjJkAiBJUoZMACRJypAJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQh\n" +
                "EwBJkjJkAiBJUoZMACRJypAJgCRJGTIBkCQpQyYAkiRlyARAkqQMmQBIkpQhEwBJkjJkAiBJUoZM\n" +
                "ACRJytAev96peCxpp9WrV4dXXnklvP/++2GPPfYo5qoKUnG1//77h2nTpoW99torPpfUPBMAqWTV\n" +
                "qlXh7rvvDps2bYrBxgSgevhd9tlnn3DEEUeEyy+/vJgrqVkmAFKBf4XrrrsubNy4Mey9994G/4ri\n" +
                "d2J6++23w2WXXRamTJlSvCKpGY4BkArLli0Lr7/+usG/4vht9txzzzBs2LDw1FNPxa4aSc0zAZAK\n" +
                "L730UmxaNvh3B5KAN998M2zevLmYI6kZJgBSgWDSW/Cnlvnee+/Fpud6+nud+X29rubxW23fvj12\n" +
                "BUhqngmAVHjnnXfqJgDvvvtuHG1+wAEHxMe1QZzATm20r9dx0EEHxddtsm4d9q37UxoYEwCpDwRs\n" +
                "AvfVV18drrrqqjB37tyYKKQgTwAaPnx4+PSnPx2uvPLKcM4553woCeD1ESNGhCuuuCJOF1xwQQxY\n" +
                "tgQMHsma+1EaOBMAqRcEF4L5RRddFA455JAwcuTIMGvWrHDMMcf0BHkCPEF97NixYdSoUeGkk04K\n" +
                "EydO7Hmdv2effXY47LDD4uenT58ez18vJwmS1AkmAFIvqKlT+z/44IOLObuceOKJ8S/Bf8KECeHI\n" +
                "I4+Mz5PTTz+9J8BzRsHkyZOLV3YhiXCgoaROMwGQekECcOihhxbPPkBtPzU/jx49Ovb/l1HTZyJB\n" +
                "GDduXDH3AwceeKAJgKSOMwGQekGA51zzWtTqweu1wR/MS5eorfd5SaoCEwCpFwTyeueYb926Nf6l\n" +
                "Fl/vFDRq/pyexuc5tbBWeRChJHWKCYDUCwL4ihUrimcfYB4BnFr+unXrPpIE8DpjAFICUHua2sqV\n" +
                "K00AJHWcCYDUC2r41NYXLVpUzNlVu1+4cGEM7ry+ZcuW8OSTTxav7vLYY499qOn/4YcfLh7tMn/+\n" +
                "fMcASOo4bwYkFbgREDX28i1m+ffYsWNHPL2PAYHz5s2L70n3C+B1WgB4/fDDDw9PPPFEWL9+fU8C\n" +
                "kF6fMWNGOProo2Pwf+2117zkcIts27YtXHPNNfFsDEnNMQGQCvUSAKQkgKZ8Anu912nyp3VgIK9r\n" +
                "4EwApIGzC0DqBzV1auxc8a9e8OZ1AvtAX5ekTjABkCQpQyYAkiRlyARAkqQMmQBIkpQhzwKQCvXO\n" +
                "AmDkPiP41XlceyGdfpl4FoA0cCYAUqE2ASD4E1jS7X3VOQR9rqfw7LPPxtMqUxJgAiANnAmAVKhN\n" +
                "ALgK4Jw5c8IZZ5wRn6uzuAfD97///Z7LLMMEQBo4xwBIfai9jr86hxYZSa1jAiBJUoZMACRJypAJ\n" +
                "gCRJGTIBkCQpQyYAkiRlyARAbbH29dfDPXfdFe+FL0nqPBMADTlOpVu0aFF466234l8vPaGccN2C\n" +
                "BfPnh2eeeaaYI1WDCYCGHBfUwZ5cYGdn8N+yZUt8LuXgpZdeCitWrAjPP/dcWL9uXTFX6jwTAA05\n" +
                "rt+ertxGa8C7O3bEx1IONr35ZvwfYNps8qsKMQHQkKPgK19f/x0TAGVk+/btPfcu2HfffeNfqQpM\n" +
                "ANQWJAGgBSB1CUg54CZGYOzLyBEj4mOpCkwA1BbDd9Z80uA/EwDlhMGvtACQ/O47fHgxV+o8EwA1\n" +
                "jYLs/SZvzDJ85Mj4l4LwrW3b4mNpd0fSm25iNHxn8E9dYVIVmACoKe/t2BFPaZo3b17Y2sSAphEj\n" +
                "RsTCMN3XXcpB6v/n2B82bFjPYNhG8Bk+Lw0VEwA1Ze0bb8RTmtatWxcWLlxYzO1fSgBgAqBc0NqV\n" +
                "BgDus+++PY8b8fzzz4cnFywI69evL+ZIrWUCoKbsePfdWIthUB9JwLJly4pX+kbzpy0Ays32ncd6\n" +
                "agHYd599Gk4A1qxZE1584YWwdu3asGTJkmKu1FomAGrKwQcfHPbZWZBRoJEEvLCzlrJx48bi1d6V\n" +
                "uwBs1lQu0gBAjn3+bxrx9s7/j8W/+lX8/2LasWNHHHcjtZoJgJpCIJ9x/PFxYBMFG3/nP/FE8Wrv\n" +
                "6P9MA6Ao0KQcbK/pAmgEY2xoJeNzBH6S7mbGDkiN8qhS0yZOnBjGT5gQgz8FEzX6Rf2MB6Aw4yIo\n" +
                "1IQo1N6yFUAZ2FZqAaAbrD8vvPBCWLd+fUyW+T8ZPXp0OO6444pXpdYyAdCAzJw5M4waNSoWUhRW\n" +
                "r776ali9enXx6kdRCFKYcWMUmjXpD5V2d+kUQBLlfYYNi497s3HDhvDSkiXx/4OEgVazU+bMKV6V\n" +
                "Ws8EQANCf+aJJ530oQKOu5311b8/ecqUMG78+DB9xgybNJUFmv1JekmU9xszppj7UfwfLV68OAZ+\n" +
                "8Jlp06eH/fbbLz6XhoKlsAZs7NixYdKkSbHwoobP4KVfLVpUvPpRhxxySJg1a1Y49thjiznS7m3y\n" +
                "5MmxD/+YY44J+++/fzH3o0ieuWkQiTH/TxMmTAhHHnlk8ao0NEwANCgMCByzs5ZCDYfCi9OXXnvt\n" +
                "teLVj2p0JLS0Oxizs9ZPMz61+d688cYbYfkrr8TbZaexAieeeGLxqjR0TAA0KPT/n3LqqbHJErQE\n" +
                "cO6ypF0Y/Npblxf/N08vXhz/b8AZMjNPPjkM96ZBagMTAA3ayJEj46BAmi5pCTjwwAOLVyT1ZcuW\n" +
                "LT3XCnhvZzLAOBm6DKR2MAFQSxw1aVIsvOjfP/zww4u5kvoyYvjw2ELAHTL335k4HzdtWvGKNPRM\n" +
                "ANQynK98/AknFM8k9YfbAzM+YNLRR8ezavby7Bi1kUebJHXQoYceGk7aGfz37+M0QWkomABIUoel\n" +
                "QYBSO5kASJKUIRMASZIyZAIgFWyG7U5cO19S8/b4dbr4tNRGXABl6dKl8W8VAi9XKHzwwQfjOdnp\n" +
                "oi2cmjV79uwwd+7c+FydtWnTpnD99dfHY6b8G82ZMydee4JrUFQB63HAAQd4OqwqzwRAHcEVz37x\n" +
                "i1/EQr23q6S1E+uwbt26nvsawASgWuolAPxeBH8SuKoUZRzb06dPjxfHkqrMLgA1jQJ4/YYNYfny\n" +
                "5WHz5s3F3OZwq1Ouk86lhHlchckugO5EFwBTvd+0ExPr0teNf6SqMAFQr9avXx9WvvpqeObpp8Pj\n" +
                "jz0W7t1ZY//5z34Wbr/ttvDwgw+GRQsXhgfuvz/WzAaCW53aANW4TjVx8xtVpXm96thXJLUjBnEt\n" +
                "f/Y1N9Ti/8r9rqFkF4A+gkJn3rx54bXVq8NeO2szNLdSO05TGc3kNHVyKeBmrVq1Kjz00EPxUqid\n" +
                "xjZS6JbHJFSlC4A7LN57771hxYoV4aijjgoXXnhhvBVzOyxbtizceeedYdu2bWHq1KnhggsuGFRw\n" +
                "G4zeugC4zXRV7jJJccq6fexjHxtwK8C8Rx8Nq3f+7/E9fN/Inft75KhRYdTOKf1lGrbzf3PU6NHF\n" +
                "p6Tm2QKgj9i6dWtY+/rr8Y5kFKw0aVKrSYUuhRLT+0V/+cgBFkJVGrhVVeznW265JbbG0GXy+s7f\n" +
                "heft2G8s64477ohBlhs+LVmyJCYi6h2/F/8vA01q2df81twSmO9genfnvDfffDOsXLkyvPD882HB\n" +
                "/Pmx5e2Xv/zlgLvgJJgA6CMI9EwEGQokalwMbKJGzB3L6Ofcb2cwGnvwwfE65mMPOqj4ZHOoSZJA\n" +
                "UGiqvhdeeCEW8uxz9hV/CRAvv/xy8Y6hs2jRovi7E9BYNsHo1Vdf9XbP/RhMApDGxPA/l5I8/heZ\n" +
                "z/fyGkk5E+95edmy+B5pIEwA9BHU9o6bOjXWQg7aGdwnHnVUDPScbnXmWWeF004/PTaNc9/yKVOm\n" +
                "9LQMDMTo0aNNAPpAwKXQTwjEBALmDzVqnCwrYdkkBG+88UYxR7U4llOyNlAnnnhiOHjs2DCsCPJv\n" +
                "v/12TL7Z9yTk5dafwfzvSR49+ggKr8k7E4CPnXtuOPW008IJJ5wQJk+eHA497LCYENAUTZJAgjBY\n" +
                "fJ/dAL2j4K/F71NvfqsRcGoDGQGOoKT62D/0zw/G+AkTwqk7k+zzzjsvXPKJT4Rzd/6dfcopYcrO\n" +
                "/8nx48fH7+e3YVnMkwbKBEB1UfBT+6PpcTC1mf6kBMBWgPoo8Kn1JSkAM3+oHXzwwR9aNjgmuMiN\n" +
                "6uNYbsUpgOUmf76Piwpxu+1T5syJifkVV14Zp6oMflR3MgFQR1G42YzZOy4ow/4hEBP8+UtQYET+\n" +
                "UKMpmoCWEjRqnQzcPOyww4p3qBb7idNbpW5gyauOYiAgtRhbAOqjq4XTEBlUtn379ri/OMWsFd0v\n" +
                "/Tn66KPDrFmz4m9DlwM1/7POOivWTlUf+4pxLVI3MAFQRxHIbMbsGwMuL7744nDRRRfFv4zJaAda\n" +
                "Hkg+Pv7xj8drD1xyySXhyCOPLF5VLYI/3WUkbVI3MAFQR6VTmmwB6NsRRxwRjj/++LbfYIYkgAGg\n" +
                "M2bMiBfcUe84hmmhsUtL3cIjVR3nqYDaHXAMM0J/KAfNSq1kAqCOY7S5pwKq26UEQOoWJgDqOAaX\n" +
                "mQCo25EA2P+vbmICoI4jAWBkud0A6lbp2PUMAHUT7waoIfP000/Hi9b01SfKa0xLly6NhWin+k8Z\n" +
                "uFXVuwFqlyrfDZBjl2ncuHHxSpn9tWjx3mOPPdaEQR1lAqAhwy2Fly9f/qHryfcmFegmAOpN1W8H\n" +
                "nJKA/opUkgNOf+3krZUl2AWgITNp0qQYTCms+5vQqeAvtQLHbyPHO++hpcDgr04zAdCQ4bKxNIdS\n" +
                "I0qFY1+T1O3qHdflCbRgtONSzlJ/TAA0ZGiWJQlwhL+0C8kw9wpoxQ2DpMFyDICG1Jo1a8IDDzzQ\n" +
                "lmvXDwZNs908BoAki0GX9JOzLc2gCCBZ4+Y/3HOgqqo+BqA/7GfWl7v6cVVHqdNMADTkbrjhhjgQ\n" +
                "MAXWKur2BOCll14Kt912W093SzNSYDrzzDPD6aefXsytnt0hAcA555wTb4MtdZpdABpyEyZMiAW1\n" +
                "hg4tLQQYAmGzE7V+/i5btqz4Ng0Ffh8G/hn8VRUmABpy3EGOwi/VgNR6dAFQ8x9oKwufM0kbOhz7\n" +
                "tFxwi2WpKkwANORoomUMgAnA0KFJfDBJFp8bNmxY8UxDgd9o4sSJxTOp80wANORo9mTUswnA0OE2\n" +
                "wYyzePvtt5uetm/fHmv/3PZXQ4P9O378+EoPslR+HASotnj55ZfD448/XtkCsNsHAeK5554LW7Zs\n" +
                "GdAgQMYAcM//Rq7a2CndOgiQ/ct6zpo1yy4AVYoJgNqCYHrzzTfHm/4MtJ96KO0OCcDurpsTAFx6\n" +
                "6aWVXk/lxy4AtQUF39ixY70okLJDksKxb/BX1ZgAqG3S2QBSTkgAHF+hKjIBUFu8+eabYcWKFWHj\n" +
                "xo1h1apV8fHq1avDhg0bwltvvWXLgLrSjh07wubNm8PatWvDypUr40RXEsc5AywJ/gyAPfjgg4tP\n" +
                "SNVhAqAhxQhzBv9973vfCwsXLoz96vSxp8FmBP/169eHdevWxfdKVcfxS2BnwOUbb7wRE4A0LoGJ\n" +
                "ZHbbtm3xmObYHjlyZKUHVypfJgAaMhSS99xzT3j00Udjwch55hSU1IwI/KmGRIFKTYpa00BGsUvt\n" +
                "xDFMyxWBH3RrkdiSwDLxOF2YicdLly6Nk1Q1ngWgIXPrrbfGy8tS+yHAczGg6dOnx0sDMyCKwpLT\n" +
                "A7mOPQUmZwjggAMOiLWmdh6angVQfVU5C4BaPccuxwlJLAP8pk2bFi/xy7FOd9fy5cvDkiVL4npx\n" +
                "bHMtjKuvvjreHVOqChMADYlnn3023H333bHWTyHJ+c8XX3xxvA5AuYZP4Uit/6c//WksOCkwKdwp\n" +
                "VNvZbGoCUH2dTgBYJjX/rVu3xuf8Pe+88+JdFGuXT7FKcnvnnXfGdQTB/9prr42PpSqwC0AtR0F9\n" +
                "33339dT8Tz311HDllVfGFoBy8AeF6pgxY8JnP/vZeJlUCncKTApaqUrotqJvP+GYPuWUU+omHxzn\n" +
                "JL1//Md/HP8PeM5AQW6NLVWFCYBa7sEHH4xBnFoQQb3RGvQnP/nJWEvic9S+aTmoTRikTqHPn2OT\n" +
                "pJZbJ0+ZMqV4pXckuL/xG78RkwQSAa7WyFgXqQpMANRSFI6c5kfBR7/nhRdeWLzSP8YA0KRKIUvg\n" +
                "p4mVx1Kn0TLFsU2XFWNYZs6cWbzSP04B5DLA6TsY8yJVgQmAWoqBT2l0PwOjRo8eXbzSmMMOOywc\n" +
                "euihsaClFYBCU+okklGa/jkmeTxnzpzilcYxTiD9L9AKIFWBCYBa6vXXX4+1dgL3ySefXMxtzkkn\n" +
                "ndRT2+Kv3QDqpNQlBYL4QG7ow2BYugw4prl2AMe11GkmAGoZCkoGOoFR/HQBDAQtAPvtt18sLG0B\n" +
                "UKfRmpUmbrs8UFwKm24u/k9IlKVOMwFQy1Cr4epnoLAbKBKHNBjQgYDqtDSglb9HHXVUMbd548eP\n" +
                "7znNlXEyUqeZAKhlKCS5QAp/uZjPQBHwR40aFR9T6EqdwrFISxTTYI9rEtt0O2xPc1UVmACoZSgg\n" +
                "U0HJOf+DwSlTfI8JgDqN4zD95QqVg0HtnwSARFnqNBMASZIyZAKglqFmk2o4XDVtMBj8x/ek+wNI\n" +
                "ncJxmP4O9rimFaEVLWRSK5gAqGUoIFPBxnX9B4oCMl1ytZ33A5BqcSyS1KbElhsBDRTHNIkt38mN\n" +
                "g6ROMwFQy3CuM6f/UVC++uqrxdzmpXupU+gyapoCU+qUdC1/WqO4y99Acepfur4Fp7pKnWYCoJah\n" +
                "kBw3blx8TAAv3zilGWvWrIl3COT7bAFQp5GIEvyZVqxYMeCElKSYQa0c19zBUOo0EwC1FAVbCtxP\n" +
                "PfVUMbc5CxcujK0JFLz8tQVAncTxnO74x/0pli1bFh83g6b/F198MX4X9waodwdBqd1MANRSU6dO\n" +
                "jff8p7bENc+pyTdj5cqVsQWA4M/32AKgTiMB5fQ/jkkez58/v3ilcSS16X9h+vTp8a/UaSYAaikC\n" +
                "P5dLpamTc53vueee4pX+0Td6//33x1pSKnR5LHUaiWi6ih8JKgG9UVz7n9YwvoMWrWOPPbZ4Reos\n" +
                "EwC13FlnnRULS4I3g6Z++ctfFq/07aabbopXSEtNrpxRYPO/qoIbAXFsEsQfeuihhu7qR9P/DTfc\n" +
                "0HNny+OPPz7sv//+xatSZ5kAqOXGjBkT7+tPgUdhuWDBghjc0y1Vy2gpIOh/5zvfiQOsqCXRipDu\n" +
                "BSBVBZfyTVcCpCXglltuCY8//njdq/pxnHPf/29961vxGOdY5l4AJMdSVeyx88C0lNWQuP3222Mh\n" +
                "SFDn9CcKUMYITJgwIdbwKThpIeA9FJgEfnC9dQradh6aFOivvfZaTFpStwO1ttmzZ4e5c+fG5+qs\n" +
                "TZs2heuvvz7+RvxeILgy8LSdg+q4FgDHLscJN6vi1NfjjjsuJq0cw6wnyeySJUvierGO3NviU5/6\n" +
                "lLV/VYoJgIYMBd+dd94ZRz+nAprCm2DPYUcBSoGZAj+FOs2s3Aq43YelCUD1VSUB4Pil1YrjIz1n\n" +
                "nThmmVi3dFyT+JLQUvM/+uij4/ulqrALQEOGAvCCCy4IZ599diwkKQxpDWB0P60B6WwBCk0KcGpQ\n" +
                "JADtDv5SMzhmCeocqyBh5PjleGbcCo9JAkhOuC32FVdcYfBXJZkAaEhRKFKL/v3f//0wZ86c+Jyx\n" +
                "ACQDIBHgsqhMvCZVHQkqSQAJAK0PtFiR2JLkMhH8OZZnzpwZg7/N/qoquwDUEXQNcGOV1NzeaXYB\n" +
                "VF9VugAakWr/JL1SVdkCoI7wfuja3TFAUKoyEwB1BDUkaXdGd4BUZSYAkjQESHLtYVWVmQCo7SwU\n" +
                "tbtjHAljFZikqjIBUNtxBkBVBv9JQ4UWALsBVGUmAGq7dNEUaXdGAmALgKrMBEBtl64BIO2u7AJQ\n" +
                "NzABUNvZAqAcpAsDSVVlAqC28xRA5YBE12NdVWYCoLazWVS7O7oAaOWyu0tVZgKgtjMBUA5IArwa\n" +
                "oKrMBEBtR63IMQDa3ZEAeMlrVZkJgNqOFgCvA6AcmACoykwA1HapBcAkQLszjm/uKClVlQmA2s6R\n" +
                "0cqBYwBUdSYAaju7AJQLEwBVmQmA2s4WAOXAFgBVnQmA2s4EQDkgAXAQoKrMBEBtRwJgF4By4Omu\n" +
                "qjITALUVBaItAMqJ9wNQVZkAqK1MAJQbxwGoqkwA1FYpAbALQDngOPdaAKoqEwC1Fc2h3gtAuSDh\n" +
                "tQVAVWUCoLayC0A5sQVAVWYCoLayC0C5sQVAVWUCoLYiAXBUtHJiC4CqygRAbeUYAOUitXJ5MSBV\n" +
                "lQmA2orgTyuAXQDKAce5Y15UVSYAaitvBKTc2OKlqjIBUFvZH6qckOzu2LEjtnpJVWMCoLayOVS5\n" +
                "oQXAga+qIhMAtRUtAHYBKBcc6yYAqioTALUVzaFSTkgAbPlSFZkAqK0cBKic2AKgKjMBUFvZBaDc\n" +
                "kAA4CFBVZAKgtrILQLkhAWCSqsYEQG1lQaicpNYuT39VFZkAqK1IAOwCUG68IZCqyARAbWVNSLkh\n" +
                "4TUBUBWZAKitGANgC4BywvFu4qsqMgFQWzkaWrkhAfCOgKoiEwC1lQmAcmQXgKrIBEBtw8VQTACU\n" +
                "G8cAqKpMANQ2XhBFOXIMgKrKBEBtk66H7iBA5cYWAFWRCYDahgTAFgDlyHsBqIpMANQ2nAJoAqDc\n" +
                "0OLFce9VMFU1JgBqG1oArAkpV94HQ1VjAqC2cRCgcmYCoKoxAVDbpEGAUm5IfD0TQFVjAqC2MQFQ\n" +
                "rkwAVEUmAGobugAcA6AckQDYBaCqMQFQ26QxAF4HQLmxBUBVZAKgtiEBMPgrN+mYtwVAVWMCoLZx\n" +
                "DIByZgKgqjEBUNvYAqBccdybAKhqTADUNmkMQFUndaf029X+nlWaYAKgqjEBUNswCIpuABKBqk2s\n" +
                "Vyqo1V04s6Teb1qliePrrbfe8hhTpeyx84D0iJR2+sEPfhA2bNgQ9tprr/ichGX27Nlh7ty58bk6\n" +
                "a9OmTeH666+PAXXPPXfVXbZt2xY+97nPhQMPPDA+l9Q4WwCkgtco6E7ealcaGBMASZIyZAIgSVKG\n" +
                "TAAkScqQCYAGjD7z+fPnh9tuuy3ceuutYdGiRV7sR5K6hAmABmTjxo3h9ttvDy+//HLYvn17HIj1\n" +
                "4osvxnlbt24t3iWpWZyYVS+R5uwHqZVMANQ0CqjHHnssntc8ZsyYcMQRR8Rpv/32i8nA448/bkuA\n" +
                "NEBLliwJTz755IduHvTqq6+GBQsWhC1bthRzpMEzAVDTqPVv3rw5jBw5MsyZMyeceuqpcTr55JPD\n" +
                "6NGjw/r168OKFSuKd0tqFAn0M888E5YtWxYWL14c561bty489dRT4ZVXXgkvvfRSnCe1ggmAmkLt\n" +
                "f+XKlfHa5tOnT//QBVjGjRsXjjvuuPiYGouk5qTrsu27774xiX7uuedi8Ge8zd57723LmlrKBEBN\n" +
                "4Xrmb775ZqzpH3744cXcD0ycODHss88+Ye3atcUcqfutWbMmPPHEEzEYr169upjbeiNGjAiHHXZY\n" +
                "7O8nyaY7gDE1PGaaOnVq8U5p8EwA1BRqIgz4o4YybNiwYu4HuEQr88v9l1I3u//++8NDDz0UW7Vo\n" +
                "mufxI488MmRXjqRbbezYsfH7aREg8POX+STeUquYAKhpqUDqTV+vSd1k3rx5sTWLVq39998/Dnod\n" +
                "Pnx4WLVqVRwIOxT4/5o8eXJPgsH/E+NtGGgrtZIJgJpCDZ8CkMFKTLXoImDiPVI3o9mfQE+zPE3v\n" +
                "Z511VpymTZsWEwJaBLh5VKtxds3zzz/fc8MjEgJuekR3gNRKJgBqCgXfAQccEAuk5cuXF3M/QBMp\n" +
                "zf8MCJS62dKlS+Ogu2OOOSYObqXbi8SW2vmkSZNiYOY9rcYFtRhnQwLAMmgJ4O+zzz47JAmH8mUC\n" +
                "oKalwX/UUqglJdSIXnjhhVhwMRhQ6mavv/56PJap8dc6+uijYzLABbFo8WoVBvy99tpr8ZbUBP6T\n" +
                "Tjopdj3QDcDAwKFIOJQvEwA1jeDORX+o6TMy+s4774zTwoUL4wDBgw46qO4ZAt2o3kBHdQYBl5pw\n" +
                "O1DzJ+COGjWq7jLpFmBAHt1grRzwSuBnufwf8X/GNGvWrPga81O3gNQKe+zMLB2xpaZR63nwwQdj\n" +
                "U2U6hCicGCR1/vnnx+fd5rrrrovbQyEMamB0edD0679J5/Eb0PWEFJR5fs0114QJEybE563C8X3T\n" +
                "TTfF61xceOGFxdwPcGxw/HNBrPPOOy8mCq3CBX8YeMio/4Tn1P6Zl45PabBMADQo9Ety5T8Oo/Hj\n" +
                "x4cpU6YUr7Qeg6MomEkyEmpKzKeZdLC1w9oEABT0/otUA79vmpKhSgBw4403xovvXHnllcWcD1Dr\n" +
                "v/fee+Pjc88910Gv6komAOoKBOIHHngg9pFy2eE0yJBzst94441w+umnDzoI1EsAVF0UXTTBf+Yz\n" +
                "nxmSBOCuu+6Kx8M555wTk9syxrtwGiDL5cwAqRvZoaSuQM2fwVH85UYp/OW0KK7KRn8tlyceLPr7\n" +
                "zYe7C8naUPWLH3XUUbG1gWvyb9q0qZi7a6AeLV8kpbvLWBflyQRAXYHgTD8rhT21Pmr+3H6YPnrQ\n" +
                "BTBY9PeaAHQPfiua3hmjMRQ41Y/BrgR/avskntyRj6sAkgQwEJD3SN3KBEBdgcA/c+bMWOviMYUy\n" +
                "o6J5TvDn3OzB4nxv+nZNArpD+u0J0kOB5PK0006LxxmD/Ricx7UvOPY4Bs8888zinVJ3MgFQ1zjy\n" +
                "yCNjkKbJn6ZZAjW1MPpoW4FzuxlbQIFvElBd/DYEf7qBuAX1UHUBgIteXX755fG4ICFg4vEll1wS\n" +
                "r9cvdTMHAaqrcPEh7pdOlwBBgG6BuXPnxkSgFbj069133x1rfHx/ecS5qoEii0BMQnjZZZcVc4de\n" +
                "SjyHMuGQ2skEQF2Dqw5ycxYKYA5bCmNq64zQJgloFQYW0tSbCnxVRyquaPqfPn26wVgaBBMAdQUO\n" +
                "0zvuuCOe9w/6fTn/PzUFc4EULz8sSY0zfVZXYHAe52RTI+fUL2r8nKbFfOZx3XZJUuNMANQV6POl\n" +
                "1k9t/5RTTomnf51wwgk987z7oCQ1xy4AdQ365OkCGDlyZDFnF07LKl8eWJLUPxMASZIyZBeAJEkZ\n" +
                "MgGQJClDJgCSJGXIBECSpAyZAEiSlCETAEmSMmQCIEkN4HoTW7duLZ5J3c8EQB2xbt26sH79+uKZ\n" +
                "VG1cLuWJJ54ICxcuLOZI3c8EQG332GOPhUcffTQ88sgjYcGCBcVcqbq46RQJK/ec2LhxY9i+fXu4\n" +
                "5557wsqVK4t3SN3HBEBt9fTTT8db7XIbX6alS5fGe/xLVcYNqLj5FInAk08+Ge66666YCLzyyivF\n" +
                "O6Tu46WA1VY333xzDPxTp06Nzwn+e++9d7jiiivi891R+hfr6y8TwYUpPU7za6feXuOuiLzG3yS9\n" +
                "1hvem97P+7i/fnp/eq088Xq9+Wnit0T5fejvbzdYvXp1ePzxx4tnu8ycOTMmBlI3MgFQ22zevDnc\n" +
                "e++9YdSoUeHCCy+M81JCcOmll4Z99903zutG5eDNlB5zA6Pyazwvv17+DPoLiAMJmH19Ji23Gf19\n" +
                "prwtTCkZSH+pTafnTOl57XuZqoJjl9ardNvpYcOGhRkzZhj81dVMANQ2FKL33XdfGDFiRLjooovi\n" +
                "PBIAbuf7yU9+Mhaq3YAgTtLC3zSlIJ8m/q34W4vgVk9v87tZb0VLvfkp8JeTAx6nv0y0LvC33Tg+\n" +
                "GbOydu3anvUkiT3//PM7sj5Sq5gAqK1uvfXW8M4774TJkyfHQLBkyZJY87/sssuKd1QPAYCAz62I\n" +
                "a4M925D+heoF8d0xsLdavSKovE9TQpAmgi7HDAlj6nIYSpz694tf/CL+/lOmTImJAKcEzp49O0ya\n" +
                "NKl4l9R9TADUVs8880xsSqUA59AjuJ544onhuOOOK95RHbRYvPXWWx8J9LUM8kOnr32eEoORI0eG\n" +
                "0aNHF68MDRLVffbZJ0ycODEsXrw4PPvss+Hwww8Pc+fOLd4hdR8TALXd/Pnzw4oVK+Jj+lBPPvnk\n" +
                "+LgK+Hcg8G/ZsiU+rg3uBvvOqy2y0vMxY8aE/fbbLz4eSrRg3XHHHXEMwLHHHlvMlbqPCYA6Il1R\n" +
                "jb7UKmG9NmzYEGuWiUG/usrFFy01Bx10UGwRkNQ/rwOgjiDwVy34g35lmnrNi7sLvxe/W7cMJJWq\n" +
                "wBYAqQbjErjSGxPNvUitALYGdF4qstJfAv/w4cPjZAIgNc4EQOoFI/9pViYR4DF/eV6bDJgUDJ3a\n" +
                "YM9fumcYRErgZ+rU6YFStzMBkBqQ/k1IBFILAY9TCwHKiUCzScHumEQ0W7SU319+nJr2a0/9M/GS\n" +
                "BscEQGqBdK2ANHG9AOYRpGr/xdr5L9dIkOzk+qT9Q2BPF/tJk8350tAyAZCGWLpaIP9q6XGaz7x0\n" +
                "nQH09hf15qH2edLb/Hp6SxTqBezyX9Q+Lr+H5nr+piZ6/jKleWm+pPYzAZAqpHzRoTSht3ngM0ma\n" +
                "R4Atv7cvvDe9Pz1P0umQaV56b5rqzSufQimpukwAJEnKkKm6JEkZMgGQJClDJgCSJGXIBECSpAyZ\n" +
                "AEiSlCETAEmSMmQCIElShkwAJEnKkAmAJEkZMgGQJClDJgCSJGXIBECSpAyZAEiSlCETAEmSMmQC\n" +
                "IElShkwAJEnKkAmAJEkZMgGQJClDJgCSJGXIBECSpAyZAEiSlCETAEmSMmQCIElShkwAJEnKkAmA\n" +
                "JEkZMgGQJClDJgCSJGXIBECSpAyZAEiSlCETAEmSMmQCIElShkwAJEnKkAmAJEkZMgGQJClDJgCS\n" +
                "JGXIBECSpAyZAEiSlCETAEmSMmQCIElShkwAJEnKkAmAJEkZ2uPXOxWPJTVh8eLF4a233gqnnnpq\n" +
                "Macxb7/9dli6dGl8zL/fgQceGA499ND4fHf2+uuvh6eeeiqcd955YZ999inmSuoUEwBl7Y033gh3\n" +
                "3313eO+998JVV10VRo8eXbzStwceeCDcfPPN8fHHP/7xOPVny5Yt4YYbbgjPPfdc2HvvvYu5Ibz/\n" +
                "/vthzJgx4corrwzTp08v5ta3Zs2a+P499tijmNO42iRj/fr14ZlnnvnQuiQUC/vuu2+YOXNm2Guv\n" +
                "vYq5u9x6661h7dq14bTTTut3fROW9S//8i/x8YQJE8Jf/MVfxMeNYF2efPLJ8PDDD4fXXnstbv9h\n" +
                "hx0WJk+eHC644IIwbNiw4p0fRYL2s5/9LJx44olhxowZxdwP4/t/9KMfhWOOOSbMmTOnmCvt/kwA\n" +
                "lLWvfvWrMUjwb3D44YeHP//zPy9e6dvf/M3fxL8EYj7/z//8z/F5bxYuXBi+973vxWBLwKoN4CQg\n" +
                "fM/s2bPDb/3Wb9UNaiQdP/nJT2JAbiYBYNvefffdcM4554RPf/rTcd6KFSvCf/7nf8Zguuee9XsC\n" +
                "33nnnRg4//AP/7CYE2LS84tf/CIMHz48ru/f/d3fhf333794tXfXX399DOIkFZs2bQpf+MIXwpQp\n" +
                "U4pX69u8eXPcbz//+c97EpK0rqz3jh074n645pprYqJSi5aWr33tazH54PMskyBfi/3wyiuvxMd8\n" +
                "1ymnnBIfS7s7xwAoWwTFDRs2hBEjRsRp9erV4Y477ihe7R2BJQVOAhCBiWDZm1/+8pcxABI0UxDj\n" +
                "c+WJxIDWB5rIeS/LKCOA8T377bdfnHhvoxPvJ0jPnz8/rFu3Ln7fgw8+GJc7atSonu2vnWiVePrp\n" +
                "p8Obb74ZPwNaL/gM28Ffkhr2Y39o/mcbWSbJTe32lbGtS5YsCd/4xjfCTTfdFPcby+Lz7DsmHrOO\n" +
                "fNcPf/jDsHXr1uLTH+A7CP4jR46M72ff1iLw06rAe+iWoFtHyoUJgLJF4ErN2ykwzZs3LwaNvqSg\n" +
                "3YiXX3453HbbbXE55aZ0avwsn0Qi4TsJRARdar1l27Zti83utc3xjSJosryXXnopPifxaeS7CJzU\n" +
                "xBPWO207r61cuTI8+uij8XlfWH5ZX/vvzjvvDN/97nfj70ACU/vZMl4jYViwYEEx5wNsI6+zrPS+\n" +
                "WnTLbN++ved9bJ+UCxMAZas2CBEQada+9957izmDQ630xhtvjMElBVsCPkGH2vW4cePifN6XEgHW\n" +
                "iUTk8ccfjzXThGBLTbiR2nY9BD8+S00aRx55ZAx8fWGdCIh0jSTlfcZj1p+uiXIiMxj01/N9rG+5\n" +
                "G4TnrAvrzG9UDtSsR71tYb/3h8/WbpOUCxMAqYRm4EWLFvX0CQ/GfffdF2vtBO8UwJg++9nPhi9+\n" +
                "8Yvhj/7oj8KXv/zlcO2118Ym8RTcU8JAX3tCkzvvIyhu3Lgx1srTVBv8WEb5dSY+Q5/71KlT43sY\n" +
                "PEf/PutHQlLG53k/6/Pbv/3bfQZF1pXvv+uuu4o5A/fYY4+FRx55JO6vlDClxIUkia6MCy+8MI5j\n" +
                "IBliuXS9sP1si6TmOAhQ2SLofuUrX4m18SQFahKBv/7rvy7mfhhB5+///u97ghTv573lU9v47r/6\n" +
                "q7/qqXHzvQxaI+AfcsghcV4Zwew//uM/Yu2WAEiNmmD/p3/6p+Hggw8u3rULg+joEiAw855nn302\n" +
                "Ds4jKLIutCyQZPBd4Lv6Os2Q72N7aG7n8yyvt8GQDHYkYUjbDraNZfGZcmtB2de//vU4xoJtY91Z\n" +
                "v+OPP754NcRxBv/+7/8e91H6btab4H/AAQeESy+9NJxwwglxfsJ4hCeeeCLMnTu37uA+zhqgRYGx\n" +
                "AvxmDLBMgyATzoL49re/HX8nln3sscd+aNCjtDuzBUAqIagSgAjIg+kKuP/++2OwSwhAV199dd3g\n" +
                "D2q3BEWCKYGPmjWBOV0voIyEhVPpxo8fHw466KA4pSZ4Ps/68x5eZ+rvGgMEPj6X8F3l5/1JiQjd\n" +
                "HQNFawe1/HLwx8knnxz+8i//8iPBH9OmTQu/93u/Vzf419PINqXlSjkwAZBqENCoTVODpIm8WQT7\n" +
                "dH4930Ut9ogjjojnzfeF93A6G+8nWBFUGUTYn9qg1UzwRrPvr4dtZcwCgyibRasDp/vRzQG2h31A\n" +
                "rZ9TIluB36HemQJlvIfET8qFCYBUIBCmYEpNlIBBE3KzSBoIhnxH+s7zzz+/eLVvqY8etAKsWrWq\n" +
                "eFZNaX8RPFlfxgLUjinoD2cm8Bk+z/4iISBZOuOMM4p3tAYtHX1hG+i6aUVCJHUDEwBpJwp9AhA1\n" +
                "/xTU6NN/8cUXw/PPPx+fN4rzz/k+AgrBjOZ6rlrXCEbnU5tO68P581XFfqLrgto6SHgI5JzG14x0\n" +
                "gSCkMQjU/juB30zKhQmAtBMBl3PwzzzzzA/1idMMT62WwNQoarSp/5/PEdTLAwT7QkBN/eApgagq\n" +
                "ujo+9rGPxf2WkiYCOWdRLF++PD5vBJdjJtkB+57af6P7S9LAmQBIBWqy3KiGUfQ8JgATjAlmDz30\n" +
                "UPGu/muJr776ak/zP9NgT1Hr66p5ncQ+omXj9NNP72k6Z98wnyv4NYLR/+mMBj7PZ2sv60tXzLJl\n" +
                "y+KAyN6m8tUKJTXGBEAqpFrsb/7mb8a/PCcwcRoZlwhOp9Wl1oF6CGZlvJcb1zSqXnKRmseriO3l\n" +
                "mgJcapjWCtaf1g8GL9IS0B/2Kefx8zn299ixY3tOnQSJxT/90z/F6/X/13/9V92JUwz/8R//Mfzq\n" +
                "V78qPiWpESYAUglBiNH43BAmdQWkoJwudkPQ6y0J4NS9FAjB99FE3iiCYUpE0FeyUQVp/biTIeud\n" +
                "9hfbzIWQUutFbVdG+hyfSdvLPJKtMi4zzD7htEYSg3oT3SZ01ZQvnDRQVd/fUiuZAEglKQBwe980\n" +
                "Kj3VarmZDk3N9H0n6f0EINCEnRDYOEe/GZyGxudYJt/NoMQqS9vPRX24iE4aaU8XCGdCcEljlJMi\n" +
                "PlOvVYP5tXcW5IqMad/2heWRfNVeFbEsjcvoDctn7EFaT2l3ZwIg1cFV8c4+++yegEIyQOBnhDsB\n" +
                "qTZIpOckCKm2S0ApX2WwEZxCmD4/kASiky6++OK4zmw3+4OgTCsA89IgP/A8BXrem/Ce2r58BlD2\n" +
                "d/oe+E4CfG8JE8uhtaAvA/m9pG5mAiD1gvvn0yedAjK1Qy4/Wx7lX4sAVA525QDXCAYQps8Q1Gov\n" +
                "A1xldJ2cdNJJPc3+7AcG8NE0Xzuqn+QAtASkfUnSUDuGgkGZdAukyx8z8Z1M7B+wv/hsfxcNKv8u\n" +
                "vWnkPdLuwqNd6gU1xk984hOxFYAgQ3CgNtrXjW9qb1+bglQjSDS45kAKiHQnEFC7CZc7TtdSICgT\n" +
                "4OnHp2WjvF9SksN7mdL+5ZTA8oWE+I6vfvWr4Utf+lL4nd/5nfC7v/u74fOf/3z4sz/7s9gSw3L4\n" +
                "LGMOJk6cWHxqF+an5ZQfl5VfL/+VcmACIPWBAMz19FPfPjXXlBAQnGql4M3rBLQ1a9bE540g+NFv\n" +
                "zuf4PM3Rvd1cp6qorXNzntrTAstjAMpImEi0UqJEUKeFpdakSZPi6YH8Hpx6WDtWgGWllpokNefz\n" +
                "Gt/PsmqxbJaZPt9fN4G0OzEBkPpxySWXxOBFkAABuhzM0nzQZZBquin4NXp9eWrKJBh8jmBE4sH3\n" +
                "dRsSAO7gl4I621PeXyjvM5KccgLw1FNPxcd9Se/vC4nC9OnTw4YNG2JXCmd21Er3X6DVgbMJTj31\n" +
                "1OIVafdnAiD1gzvR0bxcHuGfEMhqB44RSFKAIhlo5Hz49evXh8WLF8cACLoaCF7diFo0Vwhk0GQ5\n" +
                "0CfMK9e0OYMgnVlBAsSFfeq1AjSL1gi6JK655prwmc98pu6dGGmx4bLDvIep0TsLSrsDEwCpAbQC\n" +
                "1AtoBHpqu2UEbpIFar0EdO4q2Nfpabj99tvjADc+wzJoAeAKe+1QWztvBVoByk37ZWxf+doIBF2a\n" +
                "9Hkv68K2sz/6QuBuZL35bbi8M2cT9CbV/A3+yo0JgNQAWgCOO+64nosDldUGoilTpvQkC7QAbNy4\n" +
                "MXz/+98vXv2oBQsWxNvhpto/n+U++Gk8wVCrPXWutotjoLiWAlf6q91f9XC55HT2ANu9cuXKcN11\n" +
                "18Xn9XC7ZZrtWU++n+ShkeVI+oAJgNQgznMnONYGmtrnJAsMWqMmS4AisL/wwgvhu9/9bqzlE6xS\n" +
                "wLrxxhvDD3/4w9hcnYIZAZkWh6FEsP3GN74RR9P/7d/+bawFgyZ4BiN++ctfDl/5yldicjJQ1KrZ\n" +
                "F+yHsnqBmssJ05WSWgE4bZCuEy4DvGLFiri+JEacDvjTn/40/PjHP45nGPBePkPzfu1VBEH/P60J\n" +
                "9957b6/XE2CMxt133x3P7uD0QikXJgBSg2hG5rr+tQGt9qp2BK9Zs2b1tAIQpHgPtdb/9//+X7jh\n" +
                "hhvizXL+7d/+LcybN+9DwZ+ugtmzZ9ftr24lrtDHNQ1YTgr+CUkATefUxH/wgx/01MybxffMmTPn\n" +
                "Q59nG2uvCQCCP4P0yvuM/UJAZp/967/+a/jmN78Z/uEf/iE89thjsXsh7TMSANaX52VcVIjPcB2C\n" +
                "W265pW6LAuv2ne98J97rgYs8fetb34rfKeXABEAq6a/wp1k71d7B43pX6+MqggSlNHCQ4ETNnoDD\n" +
                "KPcnnngiDvyjLzwFMhILRv1z7YGhxvUG6tWYy1JXAIlCb/rbX/S/pxsFgf3V25kNXPSHBKvczUIS\n" +
                "QYLCflu9enVc51TzB9/L83PPPTc+L+PmQLRmsI85BfDpp5+O3TFltMzQ3UBCwXu46yAtDlIOTACU\n" +
                "rXLw4jFBpb9+d04toyWgfEZACka1rrrqqhjAyi0GBFVqwEy8lvAels2Fbprt+yeolvUXlEHSUu+s\n" +
                "hlp8d3nAXnlZLKf2fPx6zjjjjLpjJ2oRyNn+Aw88MLYElLHf2C/lfc268Pyyyy6Lp0zWWrVqVdzH\n" +
                "vIeJ5ddeapjrLqTfHvwuJA1SDkwAlC1q5KnJuTbQ9YVT3AiefIbP154FkMyYMSNce+21sVk/XTyo\n" +
                "3kTtlkD12c9+dkAX/iGYp+9nncqJRW/OOuusGOxoYmegXppYF7aJ76NvfNy4cXFQY8I+Yxksq69t\n" +
                "LzvttNNi7ZrPsd9qT5sso3Xgc5/7XFxuutxveV+liYSC9ePWzb2du89+4L3gL0lWOYEASQXLQPru\n" +
                "2vdIu6u9/m6n4rGUHa7Ux0TgoybZSAAmONF0TO2RJmtq+mkEfy0uQMMpcQQWmt1TTZSgQ0AiiDJQ\n" +
                "7otf/GL83oEgoHKpXZqy2Q6CYn/3EKDJm/PvCeDTpk0LU6dOjWc50NxOsGZUPvdC4Da/1L4TEgC6\n" +
                "L1h/PkMff3+o2ROM6fog6fjUpz7V5wWOWDdaDUhCli9fHvdTvX3GJYH7OnWPZT355JPxMZ+jtYLu\n" +
                "lXKCxPZwBgaJCcvgL/uvt99T2p3ssfOg77+9UNqNPfTQQzEQ1LtSXF84Da3e5WV7Q3AhCUiBmoDE\n" +
                "pW0bqUU3ggvoEDzrNYe30osvvhiXddFFFzXU2pCw/c12b4CzAdhnfJ7EgYSl0bskMtiR0f0kA1zo\n" +
                "hyv/1SKZ46wCgv7ll18eW26kHJgASNqt0WJA4lFuyahF6wLTQBIUqVuZAEiSlCEHAUqSlCETAEmS\n" +
                "MmQCIElShkwAJEnKkAmAJEkZMgGQJClDJgCSJGXIBECSpAyZAEiSlCETAEmSMmQCIElShkwAJEnK\n" +
                "kAmAJEkZMgGQJClDJgCSJGXIBECSpAyZAEiSlCETAEmSMmQCIElShkwApC6zYsWK8N577xXPOmfN\n" +
                "mjVh+/btxbOh9dprr4WtW7cWzyS1ggmAmvLmm2+G5cuXF88+6sknnwyrV68unnU/tvVrX/taeP31\n" +
                "14s5u3z7298Ot956a/FsaHzzm98Md999d/Fsl5UrV4b/+7//C//7v//btiRg8+bN4b//+7/DggUL\n" +
                "ijkhrF27Nlx33XXhe9/7XnjrrbeKuY254447wg9/+MPw/vvvF3P69sYbb4Qf/ehH4X/+53/CO++8\n" +
                "U8ztXmzD17/+9fDEE08Uc6TOMAFQU2655ZYYfFatWlXM+cDSpUvDXXfdFe69995iTvcjofn1r38d\n" +
                "XnjhhWLOrtropk2bwqJFi4o5rbd+/fqwZcuW8MwzzxRzdmH+sGHD4uN21b5fffXVuL0vvfRST9Am\n" +
                "EWS/vPvuu00F5bfffjseO2zHunXrirl9Y1ksl+1tNGmoMvYn++y5554r5kidYQKgplDojxkzJjz/\n" +
                "/PPFnA889dRTYcSIEWHvvfcu5nQ/gu2ee+75oW3i+V577TWk20mAYDns77KJEyeGcePGhUmTJsV9\n" +
                "3Q5sa5r22GOPOG/ChAnh8MMPD0cffXRT68H2EMTT9zTi0EMPDUceeWSYMmVKT/LTzXbs2BF/W6nT\n" +
                "PArVlGOOOSYW4DRFU5AlNJXTJ0yN8IQTTijmaqAItqgNlPvvv3+46qqrwtlnn93RIDJ69OhwxRVX\n" +
                "hPPOOy/ss88+xdz+sT3lqREjR44MF198cbjkkkt69ks3S79bo9svDRUTADVlxowZscDfuHFjbBJO\n" +
                "HnroodgnPXbs2DBt2rRi7gfoJ67XVEyTcK3e+raZP2/evPDII480NSCMdaW/noluit7wna1qYuZ7\n" +
                "SIi2bdtWzPkwmsBvvvnmcNttt4Vly5YVcz9A8z+BgiBBrbmcbL3yyiu9bj/7uV7XQJrHer388stx\n" +
                "GizWY8OGDcWzXZhHfz19/PX6uMvjBdiG8nb1he9ln5XRLfODH/wgLm+g3TEcG3TplLGvHnzwwfgb\n" +
                "PPvssx/q/uE3feCBB+qOg2FwJvg861vbepOwD/ht+S04ptMxx3FQ7/hkPz366KNNj7WQ+rPX3+1U\n" +
                "PJb6NXz48DgAjMKYAuu0006LwX/JkiWxdnbBBRfEJKDshhtuiO9hEBkDymg2Bs9//vOfx+fU8sBg\n" +
                "O8YYjBo1KjZ109VAIU/Lwn333RcLVgYZvvjii/FzrE9fKGAZPEZBz8R60vdMc3JCAfv9738/zJ8/\n" +
                "PyYYNDPT7AyWw/uPOOKIOIGg/vTTT8d1mjt3bpxXRoC48847Y/BgG/fbb79wyCGHFK/uCgAMnmN9\n" +
                "CKCsU3m/sP30D7M/CRSMQ6B7ZfLkybE//MYbb4yfIxkrI5lg/AXvpYUmJWIELfYhfc+PPfZYXC+W\n" +
                "SdCcPXt2fE9f2H72wwEHHBCmT58ekxJ+p5/+9KdxG4477ri4nqzTj3/847h/mAiIrO+xxx4bv4d1\n" +
                "Y0qBkQSS/U2XUnn/1OJ4u/322+PvzjazLI4D5qVlcSwyn26JRrDeJGAsn/3BxHbQrbNw4cI4n9+A\n" +
                "RImgTBLAMcoy2Z+8xu9P1wQ4Hkgw2U8cRxy37N+jjjqq59gmAea35Tv5bUl+OD6Y2C6+m2Wffvrp\n" +
                "H2odIOHld+M4Ssel1Aq2AKhpZ555Zk9TLMGIwpACi37pVNiDgv7666+PwSgV+hSKBEgQkCi8KVAT\n" +
                "ClBqULwGCl4+SyFJoKaQZ9kkIOWaWW9+8pOfxM/zmX333Td+/le/+lUMAKCWR/AnAFMT43XWr3bw\n" +
                "XSP4PPuDgE1wIPlgvzCSP9Veec9NN90UXyPYsE7gM6wD68WAO14HnydQkDTwGo9ZR/6mfQq+M+0P\n" +
                "5hNkGJAJPsc+JYCyv3md72XbScwGgvVjIqixrUhnLLBNaVxAqpmzTIJ02i6kfUSA70vaVrYh1ZbZ\n" +
                "NraBBDAlgQTPRlDj/+53vxuPO76PiX3OPkQaf8F81o9lk8iQYPE8HYO0EqTWA5Ik1offkPXls6wv\n" +
                "x0PC4Ef2edoG8F6Wxz5Iy6s9i4bWBpaZ9rPUKiYAatqBBx4Ya0sUeBSiNONTy/nYxz5WvGMXakIU\n" +
                "ZtSoqc1TgyQ4EIDB5ylIUzIBAl0K1jj++ONjwUiwZP6JJ54Yl00LAd/XFxIPaqoUnlOnTg0XXnhh\n" +
                "rEXx3RS8oFWBZaZaP9/Lskg4mkXNm5op38U+SgPkWP/UZMz+YJ+xLdTQ6UOnX59lsk5sE9t38MEH\n" +
                "x8DDZ2l5YOwFAwBT8GDfMYHaJrVpumb4HC0wPCYJIGniu9kHTKwbtXi+i8/XNqs3Ki2fie8FyQXr\n" +
                "d8opp4RPfvKTcftPPfXU+BrvmzNnThw8yGOwrtSQ2d6+pPenZbEMjjkCIsfcJz7xiZh8NtKaQYC9\n" +
                "55574nexL6jBM/GcJIDfIB2PLIv1O+yww+I+ZKK1gu3jeOe3JXFL7+U7+M34Ddl2nnNspcSEfU7L\n" +
                "E7837+Mz/La07LAcjj2WXe5aYxtJMvg92XdSK5kAaEAoeMePHx8fU5jRbElwLaNwpECjcLvsssvi\n" +
                "QC4KXWo8FGopcJSlQjTVIAl8FIIU+iQDZ511Vjj//PPDxz/+8Q+1NtRDQcqyDjrooBhoKZTpouDz\n" +
                "NDnzGrVm1oPEgqBF6wbP03n/rE+jaDVge1kvtpcpBXICMVJrCfPPPffcWPizPqwXSQPYNroW2G4m\n" +
                "Bv2xbmnf1GI/pwSGZV500UVxkB7L5Bz6tJ8JbgQvlkcgIhgyDVbtOlHzJUBefvnlH0oKTzrppBis\n" +
                "CaTgt/zUpz7Vcxw1iu1hmewPlsXve+WVVzaUAJDwcOyRBJKc8DnWk4SC7+T7wPFGIGe/s09pZeB4\n" +
                "mTVrVvwcSRT7Lr0//TYEahJNPkNiwbw03oL1ZvtnzpwZfwuO8auvvjpceuml8XWSGN5Psph+FxLK\n" +
                "lMCRiEitZAKgASHgUBhSkFE41Y78p5ZL7YeCjOBLQCDokSRQWNJUyt9GsYzy4EISA+b1hdo2y+Bz\n" +
                "6b0UogQi0O9LwCYYEHDZJloKUoAi+Da6jgRbAgTLIYBTy6NFgABMMEnjImgGppAv99+zTiQg5WWx\n" +
                "TnyO96K39aDpmOZpsA0EfpZFiwufZ734bApoJ598cnwv24ZGt68RLJPvY7u5XsT9998f90kZATJJ\n" +
                "rTzNSPuDY4l9/fDDD8dxJHRlpNf6wnHJ+6h5kzCQ6LGe/Pbg+OR1JvYlWGeOCY7llKTREsB7yvuP\n" +
                "16nlp4SGfc3r/B+U9wPfV/tZpNMc6bJZvHhxnMdYDd5H7T+1TEitYgKgAaPJkoKsHgoxXiMYpUFQ\n" +
                "SJ+pLfySet+X3t9fwK9FcEyFZz0EZ76bmldZWk5v61gPiRBBlu1joBtX62MgGcGZmnlqrUjv6682\n" +
                "V28/1JMGwdGiQVBMUqBN28D3EdCa3YfNIAFhuWwj+5auHvrNy9J28bfRbaznnHPOiX9ZFr8zrSB0\n" +
                "5/QnJWn8ZRAgiQqJIAkRCS1q16v8vLz+tZhXTmrY3zwnAUgJF3r7jtR1QCLx+OOPx9aFNDagPGhV\n" +
                "ahUTAA1YvUIwofbN69QKywgMBKXyZ8uBNvWP1tPX8upJhW5vNSdaIfjOchM0gYECuNllge2gKZk+\n" +
                "eRIgHtPEf+2118aaHd+btCoQsz/Bfit/Z6r5l7eDxwPZrkaxDp///OdjCws1ZvY7zd/0rbcao/2/\n" +
                "8IUvxJYglsO2U1smGeoP76clII2RIMDSFZDGK7QKxx8JCk39qYWhP2eccUZ8L8chZ4qw72htqE1S\n" +
                "pVYwAdCQSH34FLBJGjBIgUsBznsouNNVBXkv/egUgLXnlw9ESj7K59kzMDANfGM5BMly0OAsBApf\n" +
                "ggRTowGT94L3019MV8If/MEfxD5k+pwJ1Ok7mVK/MDjjgYRhIFKNM/VFg+8iAWPf0j/O/h5K7EOC\n" +
                "HSPeOR2OLp8/+ZM/6Ql66fz4VmBZ1Kg5c4Pflf57lgWCbRq70RsSk/SbUuNmXb/0pS/FFpp0Dj7L\n" +
                "aAVagtj3tIA1mgDwezFOg3Vke/hLtwItSwnfSfLKcSoNhgmAhgQBngBEwOXUJwI/I+sptKgpEvzT\n" +
                "ICkCBBeN4dx5CjYKy3Qa4GBQs+f7qRlSuLMMTktMzdI0mVPYE/SpnRM4OW2NAjY1uaZgUa5d10Pf\n" +
                "MROBkCZ/rtTHtnImBOfgM4F1YpkESgZ4cYEXzp1nZPpA0MVCEkBTMUGfdWc7SQjor6ZroFUJAPug\n" +
                "XnBkPkkO4xvoT2egI793GkvRaPBrBMkTxwvL43jhOOH4YlmsW3/Lou+f9/G7clYCNWuOUa5pwO9A\n" +
                "03ta74T3puOgL+wHEj2Oc04pJBlh39MK1AyurUFXAJ/luKy9sBZdK3Qx1XavSM0yAdCApUKxXoCh\n" +
                "j5vCmkKRgorztqmxUfimvlaCLLUj5hEICWAUvnwfrQHl2lwjBXAtTi+jT5rATn8vyyBApFoztXRq\n" +
                "WTSzchEWzmOnAGcd0qBGlss2sG5JWhe2r9x6QNM3BTa1exKNn/3sZ3FwGp9PgwD5XraXdSJIEKxZ\n" +
                "p9Rnn6Rl1G537XO+l75mkg2CL/uZFhXex/5lHet1afT2/fXwHtaZVhkCe5I+y+9FbZqWD95HMsO2\n" +
                "UVPnOTXYskaWWQ+fY1kkbvxGPE9XU+R35Hft74wCatgkASRq7C8CPxNJBetPcpqO57Se/H5MSPNq\n" +
                "/4J9zTHLwESSCr6H/4PaCzaVP9Mbkjd+t5QoJ3wnvy9/SbR4jzRQJgAaMApMCvk0WrqMwpCASMFM\n" +
                "UOXUJgotBuSlUfjglDUKcwpkghgBkhoPtTrmgQKTx7U1s/5Q0NOvSzLB+rB8gi2D1ZDO1acQpTZO\n" +
                "LZrHnOLFqYtg2/gcBTqtE6A5lu1ifagxJiQcTGwH65/6l6mFs51gnRgdTiBN60Rw4fTDMgp+vofX\n" +
                "y1h/lp0KfhIHTivjvQRoTn1kfxGQqUmC3ykFHQIymMdnGsF7WVeSlvIpbUjrxz5hv6bnXBiHbaSP\n" +
                "PiVc4HN8F9vQSCAEn+F7WXf+8rvxu7IP+A6OL5ZVPoOjN3wHAwhZJz7DNvGdTPwufAeP0++bsBzm\n" +
                "pa4tXisnQwnvY714jWOBU19rsWy+i+XUw2u0WPE9nPpaxr6gpYnvZ13Yl9JAeSlgDRgFEIVVvWsA\n" +
                "gJoLQZDgSmFH7YwbyJQDAoU5AZ/+ek7LorZEkKTWyESBTWHH+2oLw0ZQA0tjAQjq9PmWByaSAFD7\n" +
                "I2CRZLB8zuNOgZLPc60AmtQJOuXaOjUx3p9G9PMZmnvZPgpvukEIwgTG8v5hPajFsu8IOFwPILUQ\n" +
                "JARdvo+EqHyZXFpMOLOAfcU+AUkViRjbwXfy3QxqS2dfkMQQKAhIJAbg+9lmmsGp9faFz9DSQVcK\n" +
                "rTcsl8+zTNY/nWXBerLNjLHgu0n02Db2WcJ6cNzwXloo0n7uC+tPQkUykfY1xwjHE+NKeI3tYABd\n" +
                "IwGR/cLvnpIY9gvryXHI+hDcSSqYlwI+xy9dO2md+Q6CPadvMkiPBJckkS4Frn1AQsf3pd+gjP3N\n" +
                "Z/lt029YRhJHMz/L5rvSOiTsA45BrqNQ7/9OatQeOw/ExtJwaYDSgKXaIFclBCwCd73WjG5BICRZ\n" +
                "qvJ+3l1xWWBagxjAV7740UBw7wr+Z0gg6rUgSK1iF4CGHLWUqgel1JfezVh/g39n9das3yi6WGhZ\n" +
                "4XtoWZOGkgmAJFUEA/voxqGLpL+uGWmwTAAkaZBST+pge1TTYFjGFkhDzQRAkgaJQZGMv6g36K8Z\n" +
                "NPvT78+ZKNJQcxCgJA0SwZ97ChC4y2e5SFVmAiBJUobsApAkKUMmAJIkZcgEQJKkDJkASJKUIRMA\n" +
                "SZIyZAIgSVKGTAAkScqQCYAkSRkyAZAkKUMmAJIkZcgEQJKkDJkASJKUIRMASZIyZAIgSVKGTAAk\n" +
                "ScqQCYAkSRkyAZAkKUMmAJIkZcgEQJKkDJkASJKUIRMASZIyZAIgSVKGTAAkScqQCYAkSRkyAZAk\n" +
                "KUMmAJIkZcgEQJKkDJkASJKUIRMASZIyZAIgSVKGTAAkScqQCYAkSRkyAZAkKUMmAJIkZcgEQJKk\n" +
                "DJkASJKUIRMASZIyZAIgSVKGTAAkScqQCYAkSRkyAZAkKUMmAJIkZcgEQJKkDJkASJKUIRMASZIy\n" +
                "ZAIgSVKGTAAkScqQCYAkSRkyAZAkKUMmAJIkZcgEQJKkDJkASJKUIRMASZIyZAIgSVKGTAAkScqQ\n" +
                "CYAkSRkyAZAkKUMmAJIkZcgEQJKkDJkASJKUIRMASZIyZAIgSVKGTAAkScqQCYAkSRkyAZAkKUMm\n" +
                "AJIkZcgEQJKkDJkASJKUIRMASZKyE8L/B9FhPDRfxNLGAAAAAElFTkSuQmCC\n";
    }
}
