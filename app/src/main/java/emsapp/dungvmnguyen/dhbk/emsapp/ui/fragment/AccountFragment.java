package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by truong d on 10/9/2017.
 */

public class AccountFragment extends Fragment {
    private  View rootView;
    ArrayAdapter<String> adapter;
    Button btnChange;
    //ListView lvDemo;
    ArrayList<String> arrData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_account,container,false);

        btnChange = (Button)rootView.findViewById(R.id.btnChange);
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordFragment();
            }
        });

        binding_data();

        return rootView;
    }
    private void binding_data (){
        Call<HashMap> response = RestClient.getInstance().getUserService().getProfile(new TokenDTO(Global.getToken()));

        response.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap res = response.body();
                String id = "";
                String fullname = "";
                String type = "";
                String username = "";
                JSONObject json = new JSONObject(res);

                if (json.has("message")){
                    try {
                        if (json.get("message").equals(1)){
                            if (json.has("full_name")){
                                fullname = (String) json.get("full_name").toString();
                            }
                            if (json.has("type")){
                                type = (String) json.getString("type").toString();
                            }
                            if (json.has("username")){
                                username = (String) json.getString("username").toString();
                            }
                            if (json.has("id")){
                                id = (String) json.getString("id").toString();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                TextView name = rootView.findViewById(R.id.tv_name);
                TextView usertype = rootView.findViewById(R.id.tv_usertype);
                TextView user = rootView.findViewById(R.id.tv_username);
                TextView userid = rootView.findViewById(R.id.tv_id);

                name.setText(fullname.toUpperCase());
                usertype.setText("  TYPE: " + type);
                user.setText("  USER NAME: " + username);
                userid.setText("  ID: " + id);
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {
                Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changePasswordFragment (){
      getFragmentManager().beginTransaction().replace(R.id.ac_account,new ChangePasswordFragment()).addToBackStack(null).commit();
    }
}
