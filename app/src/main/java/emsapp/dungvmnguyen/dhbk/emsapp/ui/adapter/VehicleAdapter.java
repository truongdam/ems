package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.Vehicle;

/**
 * Created by WINDOWS on 10/27/2017.
 */

public class VehicleAdapter extends ArrayAdapter<Vehicle> {

    public VehicleAdapter(Context context, int resource, List<Vehicle> items) {
        super(context, resource, items);
    }

    public VehicleAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.vehicle_row, null);
        }
        Vehicle p = getItem(position);
        if (p != null) {
            TextView vehicleId = (TextView) view.findViewById(R.id.txtId);
            vehicleId.setText(String.format("ID: %s", p.getId()));

            TextView type = (TextView) view.findViewById(R.id.txtType);
            type.setText(String.format("Name: %s", p.getName()));
        }
        return view;
    }
}
