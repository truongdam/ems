package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.Client;

/**
 * Created by WINDOWS on 11/4/2017.
 */

public class ClientAdapter extends ArrayAdapter<Client> {
    public ClientAdapter(Context context, int resource, List<Client> items) {
        super(context, resource, items);
    }

    public ClientAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.client_row, null);
        }
        Client p = getItem(position);
        if (p != null) {
            TextView fullName = (TextView) view.findViewById(R.id.txtFullName);
            fullName.setText(" " + p.getName());

            TextView email = (TextView) view.findViewById(R.id.txtEmail);
            email.setText(" " + p.getEmail());

            TextView phone = (TextView) view.findViewById(R.id.txtPhone);
            phone.setText(" " + p.getPhone_number());
        }
        return view;
    }
}
