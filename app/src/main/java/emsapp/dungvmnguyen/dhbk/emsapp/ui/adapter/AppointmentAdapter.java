package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.Appointment;

/**
 * Created by WINDOWS on 10/7/2017.
 */

public class AppointmentAdapter extends ArrayAdapter<Appointment> {

    public AppointmentAdapter(Context context, int resource, List<Appointment> items) {
        super(context, resource, items);
    }

    public AppointmentAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.apponitment_row, null);
        }
        Appointment p = getItem(position);
        if (p != null) {
            TextView customerName = (TextView) view.findViewById(R.id.txtName);
            customerName.setText("  " + p.getName());

            TextView destination = (TextView) view.findViewById(R.id.txtDestination);
            destination.setText("  " + p.getDestination());

            TextView date = (TextView) view.findViewById(R.id.txtDate);
            date.setText("  " + p.getStart_date());

            TextView clientName = (TextView) view.findViewById(R.id.txtClientName);
            clientName.setText("  " + p.getClient_name());

//            TextView clientEmail = (TextView) view.findViewById(R.id.txtClientEmail);
//            clientEmail.setText("  " + p.getClient_email());
        }
        return view;
    }
}
