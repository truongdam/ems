package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by truong d on 10/17/2017.
 */

public class CoordinateDTO {

    private Double pLong;
    private Double pLat;

    private String time;

    public CoordinateDTO(Double pLong, Double pLat, String time) {
        this.pLong = pLong;
        this.pLat = pLat;
        this.time = time;
    }

    public Double getpLong() {
        return pLong;
    }

    public void setpLong(Double pLong) {
        this.pLong = pLong;
    }

    public Double getpLat() {
        return pLat;
    }

    public void setpLat(Double pLat) {
        this.pLat = pLat;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
