package emsapp.dungvmnguyen.dhbk.emsapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentCompleteDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputCostActivity extends AppCompatActivity {

    Button btnSend;
    EditText et_cost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_cost);

        et_cost = (EditText) findViewById(R.id.edtCost);

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSend.setEnabled(false);

                Double cost = Double.valueOf(et_cost.getText().toString().trim());
                Call<HashMap> endAppointment = RestClient.getInstance().getAppointmentService().endAppointment(new AppointmentCompleteDTO(Global.getAppointmentId(), "01:15 15-05-2017", Global.getToken(),cost));
                endAppointment.enqueue(new Callback<HashMap>() {
                    @Override
                    public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                        HashMap res = response.body();
                        JSONObject json = new JSONObject(res);

                        if (json.has("message")){
                            try {
                                if (json.get("message").equals(1)){
                                    Toast.makeText(InputCostActivity.this, "Appointment finished successfully", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(InputCostActivity.this, "Appointment finished fail", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        showActivity(AppointmentActivity.class);
                    }
                    @Override
                    public void onFailure(Call<HashMap> call, Throwable t) {
                        Toast.makeText(InputCostActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void showActivity(Class activity) {
        Intent intent = new Intent(InputCostActivity.this, activity);
        startActivity(intent);
    }
}
