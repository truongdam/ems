package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by truong d on 10/26/2017.
 */

public class PasswordDTO {

    String json_token;

    String old_pass;

    String new_pass;

    public PasswordDTO(String json_token, String old_pass, String new_pass) {
        this.json_token = json_token;
        this.old_pass = old_pass;
        this.new_pass = new_pass;
    }

    public String getNew_pass() {
        return new_pass;
    }

    public void setNew_pass(String new_pass) {
        this.new_pass = new_pass;
    }

    public String getOld_pass() {
        return old_pass;
    }

    public void setOld_pass(String old_pass) {
        this.old_pass = old_pass;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }
}
