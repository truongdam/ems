package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.User;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.ListUsersAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagedUsersFragment extends Fragment {

    ListView lvManagedUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_managed_users, container, false);

        Call<HashMap> getManagedUsersByOneManager = RestClient.getInstance().getUserService().getManagedUsersByOneManager(new TokenDTO(Global.getToken()));
        getManagedUsersByOneManager.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap listEmployeesObject = response.body();
                ArrayList listEmployees = (ArrayList) listEmployeesObject.get("listEmployees");
                Integer message = (Integer) listEmployeesObject.get("message");

                if (message == 1 ) {

                    final ArrayList<User> employees = new ArrayList<>();
                    lvManagedUser = (ListView) view.findViewById(R.id.lvUser);

                    try {
                        Iterator itr = listEmployees.iterator();

                        while(itr.hasNext()) {
                            HashMap employee = (HashMap) itr.next();
                            employees.add(new User((String) employee.get("username"), (String) employee.get("fullname"), (String) employee.get("email"), (String) employee.get("type"), (Integer) employee.get("id"), (Integer) employee.get("status"), false));
                        }

                        final ListUsersAdapter adapter = new ListUsersAdapter(
                                getContext(),
                                R.layout.managed_user_row,
                                employees
                        );

                        lvManagedUser.setAdapter(adapter);

                    }catch (Exception e){

                    }

                }
                else {
                    Toast.makeText(getContext(), "Get list users unsuccessful.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {

            }
        });

        return view;
    }

}
