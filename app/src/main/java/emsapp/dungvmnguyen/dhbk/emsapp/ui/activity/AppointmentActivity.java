package emsapp.dungvmnguyen.dhbk.emsapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment.AppointmentFragment;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment.ClientFragment;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment.HistoryFragment;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment.ManagedUsersFragment;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment.NewAppointmentFragment;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.util.TypefaceUtil;

public class AppointmentActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Roboto-Regular.ttf");

        ArrayList<Integer> a = new ArrayList<>();

        Global.setUserIds(a);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Global.getType().equals("Manager")) {
            ManagedUsersFragment managedUsersFragment = new ManagedUsersFragment();
            showFragment(managedUsersFragment);
        } else {
            AppointmentFragment appointmentFragment = new AppointmentFragment();
            showFragment(appointmentFragment);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        Fragment fragment = null;

        switch (id){
            case R.id.newAppointment: {
                if (Global.getType().equals("Manager")) {
                    fragment = new NewAppointmentFragment();
                    break;
                } else {
                    Toast.makeText(this, "You are not privileged to use this feature", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
            case R.id.customer: {
                if (Global.getType().equals("Manager")) {
                    fragment = new ClientFragment();
                    break;
                } else {
                    Toast.makeText(this, "You are not privileged to use this feature", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
            case R.id.appointment: {
                if (Global.getType().equals("Manager")) {
                    fragment = new ManagedUsersFragment();
                    break;
                } else {
                    fragment = new AppointmentFragment();
                    break;
                }
            }
            case R.id.info: {
                showActivity(AccountActivity.class);
                return true;
            }
            case R.id.logout: {
                Global.setToken(null);
                showActivity(LoginActivity.class);
                return true;
            }
            case R.id.history: {
                fragment = new HistoryFragment();
                break;
            }
            case R.id.about: {
                return true;
            }
        }

        showFragment(fragment);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showActivity(Class activity) {
        Intent intent = new Intent(getApplicationContext(), activity);
        startActivity(intent);
    }

    private void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }
}
