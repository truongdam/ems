package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.PasswordDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by truong d on 10/9/2017.
 */

public class ChangePasswordFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.fragment_changepassword,container,false);

        binding_data(rootview);
        bind_envents(rootview);
        return rootview;
    }

    private  void  binding_data(final View rootview){
        Call<HashMap> response = RestClient.getInstance().getUserService().getProfile(new TokenDTO(Global.getToken()));

        response.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap res = response.body();
                String fullname = "";
                String type = "";
                String username = "";
                String id = "";
                JSONObject json = new JSONObject(res);

                if (json.has("message")){
                    try {
                        if (json.get("message").equals(1)){
                            if (json.has("full_name")){
                                fullname = (String) json.get("full_name").toString();
                            }
                            if (json.has("type")){
                                type = (String) json.getString("type").toString();
                            }
                            if (json.has("username")){
                                username = (String) json.getString("username").toString();
                            }
                            if (json.has("id")){
                                id = (String) json.getString("id").toString();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                TextView name = (TextView) rootview.findViewById(R.id.tv_name);
                TextView usertype = (TextView) rootview.findViewById(R.id.tv_usertype);
                TextView user = (TextView) rootview.findViewById(R.id.tv_username);
                TextView userid = (TextView)rootview.findViewById(R.id.tv_id);

                name.setText(fullname.toUpperCase());
                usertype.setText("  TYPE: " + type);
                user.setText("  USER NAME: " + username);
                userid.setText("  ID: " + id);
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {
            }
        });
    }
    private  void bind_envents (final View rootview){

        Button btn_save = rootview.findViewById(R.id.btnChangePassword);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oPassword =  (String)((EditText) rootview.findViewById(R.id.et_currentpassword)).getText().toString();
                String nPassword = (String)((EditText) rootview.findViewById(R.id.et_newpassword)).getText().toString();
                String cPassword = (String)((EditText) rootview.findViewById(R.id.et_confirmpassword)).getText().toString();
                boolean isvalid = true;

                if (StringUtils.isEmpty(oPassword)){
                    isvalid = false;
                }

                if ( StringUtils.isEmpty(nPassword)){
                    isvalid = false;
                }

                if (StringUtils.isEmpty(cPassword)){
                    isvalid =false;
                }
                if (isvalid){
                    if(nPassword.equals(cPassword)){

                        Call<HashMap> response = RestClient.getInstance().getUserService()
                                .changePassword( new PasswordDTO(Global.getToken(),oPassword,nPassword));
                        response.enqueue(new Callback<HashMap>() {
                            @Override
                            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                                HashMap res = response.body();
                                Integer mes = (Integer) res.get("message");
                                if (mes.equals(1)){
                                    String token = (String) res.get("new_json_token");
                                    Global.setToken(token);
                                    Toast.makeText(getContext(),"Change password successfully", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(getContext(),"Change password unsuccessfully",Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<HashMap> call, Throwable t) {
                                Toast.makeText(getContext(),"Change password unsuccessfully", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    else {

                        Toast.makeText(getContext(), "Password's not match", Toast.LENGTH_SHORT).show();

                    }
                }

                else
                {
                    Toast.makeText(getContext(),"Please enter input", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
}
