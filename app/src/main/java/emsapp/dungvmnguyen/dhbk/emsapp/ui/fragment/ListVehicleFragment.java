package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.Vehicle;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.VehicleAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by WINDOWS on 10/22/2017.
 */

public class ListVehicleFragment extends Fragment {
    ListView lvVehicles;
    Button btnAccept;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list_vehicle, container, false);

        Call<ArrayList> getVehicle = RestClient.getInstance().getAppointmentService().getVehicle();
        getVehicle.enqueue(new Callback<ArrayList>() {
            @Override
            public void onResponse(Call<ArrayList> call, Response<ArrayList> response) {
                lvVehicles = (ListView) view.findViewById(R.id.lvVehicle);
                final ArrayList<Vehicle> vehicleArrayList = new ArrayList<>();

                ArrayList vehicles = response.body();
                Iterator iterator = vehicles.iterator();
                while (iterator.hasNext()) {
                    HashMap vehicle = (HashMap) iterator.next();
                    vehicleArrayList.add(new Vehicle((String) vehicle.get("id").toString(), (String) vehicle.get("name").toString()));
                }

                VehicleAdapter adapter = new VehicleAdapter(
                        getContext(),
                        R.layout.vehicle_row,
                        vehicleArrayList
                );

                lvVehicles.setAdapter(adapter);

                lvVehicles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ActivationFragment activationFragment = new ActivationFragment();
                        showFragment(activationFragment, vehicleArrayList.get(position).getId(), vehicleArrayList.get(position).getName());
                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList> call, Throwable t) {
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }

    private void showFragment(Fragment newFragment, String vehicleId, String vehicleName) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundleobj = new Bundle();
        bundleobj.putCharSequence("vehicleId", vehicleId);
        bundleobj.putCharSequence("vehicleName", vehicleName);
        newFragment.setArguments(bundleobj);
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
