package emsapp.dungvmnguyen.dhbk.emsapp.rest.service;

import java.util.ArrayList;
import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentCompleteDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDetailCoordinateDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDetailDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDetailFinishDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.DetailDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppointmentService {
    @POST("/api/getActiveAppointments")
    Call<HashMap> getAllAppointment(@Body TokenDTO tokenDTO);

    @POST("/api/createAppointment")
    Call<HashMap> createAppointment(@Body AppointmentDTO appointmentDTO);

    @POST("/api/createDetail")
    Call<HashMap> createDetail(@Body AppointmentDetailDTO appointmentDetailDTO);

    @POST("/api/detail/end")
    Call<HashMap> endDetail(@Body AppointmentDetailFinishDTO appointmentDetailFinishDTO);

    @POST("/api/detail/addCoordinate/str")
    Call<HashMap> addCoordinate(@Body AppointmentDetailCoordinateDTO appointmentDetailCoordinateDTO);

    @GET("/api/getVehicles")
    Call<ArrayList> getVehicle();

    @POST("/api/endAppointment ")
    Call<HashMap> endAppointment(@Body AppointmentCompleteDTO appointmentCompleteDTO);

    @POST("/api/detail/addCoordinate")
    Call<HashMap> addCoordinate2(@Body DetailDTO detailDTO);
}
