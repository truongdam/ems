package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.History;

/**
 * Created by truong d on 10/14/2017.
 */

public class HistoryAdapter extends ArrayAdapter<History> {
    public HistoryAdapter(Context context, int resource, List<History> items) {
        super(context, resource, items);
    }

    public HistoryAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.history_item, null);
        }
        History p = getItem(position);
        if (p != null) {
            TextView customerName = (TextView) view.findViewById(R.id.txtName);
            customerName.setText("  " + p.getName());

            TextView destination = (TextView) view.findViewById(R.id.txtDestination);
            destination.setText("  " + p.getDestination());

            TextView date = (TextView) view.findViewById(R.id.txtDate);
            date.setText("  " + p.getEndDate());

            TextView money = (TextView)view.findViewById(R.id.textMoney);
            money.setText("  " + Integer.toString(p.getTotal_cost()));
        }
        return view;
    }
}
