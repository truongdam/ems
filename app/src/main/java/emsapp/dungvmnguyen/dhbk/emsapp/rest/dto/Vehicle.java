package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/22/2017.
 */

public class Vehicle {

    private String id;
    private String name;

    public Vehicle(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
