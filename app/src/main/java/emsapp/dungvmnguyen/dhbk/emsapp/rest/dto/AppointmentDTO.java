package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

import java.util.ArrayList;

/**
 * Created by WINDOWS on 10/21/2017.
 */

public class AppointmentDTO {
    private String json_token;
    private String name;
    private Integer client_id;
    private String destination;
    private String start_date;
    private ArrayList<Integer> users;

    public AppointmentDTO(String json_token, String name, Integer client_id, String destination, String start_date, ArrayList<Integer> users) {
        this.json_token = json_token;
        this.name = name;
        this.client_id = client_id;
        this.destination = destination;
        this.start_date = start_date;
        this.users = users;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClient_id() {
        return client_id;
    }

    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public ArrayList<Integer> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<Integer> users) {
        this.users = users;
    }
}
