package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by truong d on 10/29/2017.
 */

public class GPSTime {
    private String time;
    private Double latitude;
    private Double longitude;

    public GPSTime(String time, Double latitude, Double longitude) {
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
