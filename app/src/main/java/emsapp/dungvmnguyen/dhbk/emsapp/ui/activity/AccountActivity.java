package emsapp.dungvmnguyen.dhbk.emsapp.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment.AccountFragment;

public class AccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        AccountFragment accountFragment = new AccountFragment();
        showFragment(accountFragment);
    }

    private void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.ac_account, fragment)
                .commit();
    }
}
