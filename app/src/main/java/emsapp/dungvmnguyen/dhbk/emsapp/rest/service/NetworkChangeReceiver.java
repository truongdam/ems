package emsapp.dungvmnguyen.dhbk.emsapp.rest.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import emsapp.dungvmnguyen.dhbk.emsapp.core.NetworkUtil;

/**
 * Created by truong d on 11/24/2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String status = NetworkUtil.getConnectivityStatusString(context);
        Toast.makeText(context,status,Toast.LENGTH_SHORT).show();
    }
}
