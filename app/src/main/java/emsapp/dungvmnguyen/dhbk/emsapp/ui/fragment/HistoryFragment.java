package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.History;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.HistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by truong d on 10/14/2017.
 */

public class HistoryFragment extends Fragment {
    ListView lv_history;
    HistoryAdapter adapter;
    ArrayList<History> listHistory;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history,container,false);
        lv_history= (ListView) view.findViewById(R.id.lv_history);

        Call<HashMap> historyAppointment = RestClient.getInstance().getHistoryAppointmentService().getAllHistoryAppointment(new TokenDTO(Global.getToken()));
        historyAppointment.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap res = response.body();
                Integer message = (Integer) res.get("message");
                listHistory = new ArrayList<History>();
                ArrayList listHistory = (ArrayList) res.get("completedAppoiments");
                if ( message == 1){
                    Iterator itr = listHistory.iterator();
                    while(itr.hasNext()) {
                        HashMap history = (HashMap) itr.next();
                        HistoryFragment.this.listHistory.add(new History((String) history.get("name"), (String) history.get("destination"), (Integer) history.get("id"), (String) history.get("start_date"), (Integer) history.get("total_cost"), (String) history.get("end_date")));
                    }

                    adapter = new HistoryAdapter(
                            getContext(),
                            R.layout.history_item,
                            HistoryFragment.this.listHistory
                    );

                    lv_history.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {

            }
        });
        return view;
    }
}
