package emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.User;

/**
 * Created by WINDOWS on 11/4/2017.
 */

public class ListUsersAdapter extends ArrayAdapter<User> {
    public ListUsersAdapter(Context context, int resource, List<User> items) {
        super(context, resource, items);
    }

    public ListUsersAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.managed_user_row, null);
        }
        User p = getItem(position);
        if (p != null) {
            TextView fullName = (TextView) view.findViewById(R.id.txtFullName);
            fullName.setText(" " + p.getFullname());

            TextView email = (TextView) view.findViewById(R.id.txtEmail);
            email.setText(" " + p.getEmail());

            TextView type = (TextView) view.findViewById(R.id.txtType);
            type.setText(" " + p.getType());
        }
        return view;
    }
}
