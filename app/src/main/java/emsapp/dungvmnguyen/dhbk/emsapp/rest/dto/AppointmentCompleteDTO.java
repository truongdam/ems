package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/28/2017.
 */

public class AppointmentCompleteDTO {
    private Integer id;
    private String end_date;
    private String json_token;

    public AppointmentCompleteDTO(Integer id, String end_date, String json_token, Double total_cost) {
        this.id = id;
        this.end_date = end_date;
        this.json_token = json_token;
        this.total_cost = total_cost;
    }

    public Double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(Double total_cost) {
        this.total_cost = total_cost;
    }

    private  Double total_cost;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }
}
