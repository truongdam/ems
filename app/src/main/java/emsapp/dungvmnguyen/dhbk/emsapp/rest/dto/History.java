package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by truong d on 10/14/2017.
 */

public class History {
    private String name;
    private String destination;
    private Integer id;
    private String startDate;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    private String endDate;

    public History(String name, String destination, Integer id, String startDate, Integer total_cost, String endDate) {
        this.name = name;
        this.destination = destination;
        this.id = id;
        this.startDate = startDate;
        this.total_cost = total_cost;
        this.endDate = endDate;
    }


    public Integer getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(Integer total_cost) {
        this.total_cost = total_cost;
    }

    private Integer total_cost;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}
