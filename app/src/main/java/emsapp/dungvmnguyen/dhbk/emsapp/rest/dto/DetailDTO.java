package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by truong d on 10/29/2017.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "json_token",
        "detail_id",
        "coordinates"
})
public class DetailDTO {

    @JsonProperty("json_token")
    private String jsonToken;
    @JsonProperty("detail_id")
    private Integer detailId;
    @JsonProperty("coordinates")
    private List<CoordinateDTO2> coordinates = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public DetailDTO(String jsonToken, Integer detailId, List<CoordinateDTO2> coordinates) {
        this.jsonToken = jsonToken;
        this.detailId = detailId;
        this.coordinates = coordinates;
    }

    @JsonProperty("json_token")
    public String getJsonToken() {
        return jsonToken;
    }

    @JsonProperty("json_token")
    public void setJsonToken(String jsonToken) {
        this.jsonToken = jsonToken;
    }

    @JsonProperty("detail_id")
    public Integer getDetailId() {
        return detailId;
    }

    @JsonProperty("detail_id")
    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    @JsonProperty("coordinates")
    public List<CoordinateDTO2> getCoordinates() {
        return coordinates;
    }

    @JsonProperty("coordinates")
    public void setCoordinates(List<CoordinateDTO2> coordinates) {
        this.coordinates = coordinates;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}