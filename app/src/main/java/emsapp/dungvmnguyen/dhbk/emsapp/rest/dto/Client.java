package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 11/4/2017.
 */

public class Client {
    private String address;
    private String name;
    private String phone_number;
    private Integer id;
    private String email;

    public Client(String address, String name, String phone_number, Integer id, String email) {
        this.address = address;
        this.name = name;
        this.phone_number = phone_number;
        this.id = id;
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
