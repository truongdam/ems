package emsapp.dungvmnguyen.dhbk.emsapp.rest.service;

import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ClientService {
    @POST ("api/getClients")
    Call<HashMap> getClients(@Body TokenDTO tokenDTO);
}
