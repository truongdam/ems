package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import emsapp.dungvmnguyen.dhbk.emsapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewAppointmentListButtonFragment extends Fragment {

    Button btnSave;
    EditText edtName, edtDes, edtDate, edtUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_appointment_list_button, container, false);

        return view;
    }

}
