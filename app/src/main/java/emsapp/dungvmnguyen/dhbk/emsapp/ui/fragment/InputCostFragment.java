package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.AppointmentDetailFinishDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


/**
 * Created by truong d on 10/14/2017.
 */

public class InputCostFragment extends DialogFragment {
    EditText et_cost;
    ImageView img_camera, img_gallery, img_upload, img_star;
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;
    final int REQUEST_CODE_CAMERA = 13323;
    final int REQUEST_CODE_GALLERY = 22131;
    String selectedPhoto;
    private final String TAG = this.getClass().getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_inputcost,container,false);

        et_cost = (EditText) view.findViewById(R.id.edtCost);

        cameraPhoto = new CameraPhoto(getContext());
        galleryPhoto = new GalleryPhoto(getContext());

        img_camera = (ImageView) view.findViewById(R.id.img_camera);
        img_gallery = (ImageView) view.findViewById(R.id.img_gallery);
        img_upload = (ImageView) view.findViewById(R.id.img_upload);
        img_star = (ImageView) view.findViewById(R.id.img_star);


        img_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!runtime_permissions()) {
                        startActivityForResult(cameraPhoto.takePhotoIntent(), REQUEST_CODE_CAMERA);
                        cameraPhoto.addToGallery();
                    }
                } catch (IOException e) {
                    Toast.makeText(getContext(), "Something wrong while taking photo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        img_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), REQUEST_CODE_GALLERY);
            }
        });

        img_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_cost.getText().toString().trim().equals("")) {
                    Toast.makeText(getContext(), "Please input your cost.", Toast.LENGTH_SHORT).show();
                }
                else {
                    final Double cost = Double.valueOf(et_cost.getText().toString().trim());

                    DateFormat formatter = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
                    String date = (String) formatter.format(new Date(System.currentTimeMillis()));

                    try {
                        String encodeImage;
                        if (selectedPhoto == null) {
                            encodeImage = Global.getEmpty_img();
                        } else {
                            Bitmap bitmap = ImageLoader.init().from(selectedPhoto).requestSize(1024, 1024).getBitmap();
                            encodeImage = ImageBase64.encode(bitmap);
                            Log.d(TAG, encodeImage);
                        }
                        Call<HashMap> endDetail = RestClient.getInstance().getAppointmentService().endDetail(new AppointmentDetailFinishDTO(Global.getToken(), Global.getDetail_id(), date ,cost, "", "end detail", encodeImage));
                        endDetail.enqueue(new Callback<HashMap>() {
                            @Override
                            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                                HashMap res = response.body();
                                showFragment();
                            }

                            @Override
                            public void onFailure(Call<HashMap> call, Throwable t) {
                                Toast.makeText(getContext(), "Connection failed", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (FileNotFoundException e) {
                        Toast.makeText(getContext(), "Something wrong while encoding photo", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_CAMERA) {
                String photoPath = cameraPhoto.getPhotoPath();
                selectedPhoto = photoPath;
                Bitmap bitmap = null;
                try {
                    bitmap = ImageLoader.init().from(photoPath).requestSize(1024,1024).getBitmap();
                    img_star.setImageBitmap(bitmap);
                } catch (IOException e) {
                    Toast.makeText(getContext(), "Something wrong while loading photo", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == REQUEST_CODE_GALLERY) {
                Uri uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                String photoPath = galleryPhoto.getPath();
                selectedPhoto = photoPath;
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(1024,1024).getBitmap();
                    img_star.setImageBitmap(bitmap);
                } catch (IOException e) {
                    Toast.makeText(getContext(), "Something wrong while choosing photo", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private void showFragment() {
        Fragment fr = new ActivationFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Global.setCheckExistCost(true);
        transaction.replace(R.id.content_frame, fr);
        transaction.commit();
    }

    private boolean runtime_permissions(){
        if(Build.VERSION.SDK_INT >=23 && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);
            return true;
        }
        return false;
    };
}
