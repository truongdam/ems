package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.Client;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.ClientAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientFragment extends Fragment {

    ListView lvClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_client, container, false);

        Call<HashMap> getClients = RestClient.getInstance().getClientService().getClients(new TokenDTO(Global.getToken()));
        getClients.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap listClientsObject = response.body();
                ArrayList listClients = (ArrayList) listClientsObject.get("client_list");
                Integer message = (Integer) listClientsObject.get("message");

                if (message == 1 ) {
                    final ArrayList<Client> clients = new ArrayList<>();
                    lvClient = (ListView) view.findViewById(R.id.lvClient);

                    try {
                        Iterator itr = listClients.iterator();

                        while(itr.hasNext()) {
                            HashMap client = (HashMap) itr.next();
                            clients.add(new Client((String) client.get("address"), (String) client.get("name"), (String) client.get("phone_number"), (Integer) client.get("id"), (String) client.get("email")));
                        }

                        final ClientAdapter adapter = new ClientAdapter(
                                getContext(),
                                R.layout.client_row,
                                clients
                        );

                        lvClient.setAdapter(adapter);

                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {

            }
        });

        return view;
    }
}
