package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

import java.util.ArrayList;

/**
 * Created by WINDOWS on 10/26/2017.
 */

public class AppointmentDetailCoordinateDTO {

    private String json_token;
    private Integer detail_id;
    private ArrayList<String> coordinates;

    public AppointmentDetailCoordinateDTO(String json_token, Integer detail_id, ArrayList<String> coordinates) {
        this.json_token = json_token;
        this.detail_id = detail_id;
        this.coordinates = coordinates;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }

    public Integer getDetail_id() {
        return detail_id;
    }

    public void setDetail_id(Integer detail_id) {
        this.detail_id = detail_id;
    }

    public ArrayList<String> getCoordinate() {
        return coordinates;
    }

    public void setCoordinate(ArrayList<String> coordinates) {
        this.coordinates = coordinates;
    }
}
