package emsapp.dungvmnguyen.dhbk.emsapp.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by truong d on 11/24/2017.
 */

public class NetworkUtil {
    public static int TYPE_WIFI = 1;
    public static  int TYPE_3G = 2;
    public static  int TYPE_NOT_INTERNET = 3;

    public static int getConnectivityStatus (Context context){
        int result = TYPE_NOT_INTERNET;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null){
            if ( activeNetwork.getType() == ConnectivityManager.TYPE_WIFI )
                result = TYPE_WIFI;
            else if ( activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                result = TYPE_3G;
        }
        return result;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        String status = null;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetworkUtil.TYPE_3G) {
            status = "Mobile data enabled";
        } else if (conn == NetworkUtil.TYPE_NOT_INTERNET) {
            status = "Not connected to Internet";
        }
        return status;
    }

}
