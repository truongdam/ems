package emsapp.dungvmnguyen.dhbk.emsapp.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emsapp.dungvmnguyen.dhbk.emsapp.R;
import emsapp.dungvmnguyen.dhbk.emsapp.core.Global;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.RestClient;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.TokenDTO;
import emsapp.dungvmnguyen.dhbk.emsapp.rest.dto.User;
import emsapp.dungvmnguyen.dhbk.emsapp.ui.adapter.UserAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListUserFragment extends Fragment {

    ListView lvUser;
//    ImageView img_accept, img_cancel;
    Button btnSave, btnBack;
    ArrayList<Integer> ids = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_list_user, container, false);

        Call<HashMap> getManagedUsersByOneManager = RestClient.getInstance().getUserService().getManagedUsersByOneManager(new TokenDTO(Global.getToken()));
        getManagedUsersByOneManager.enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap listEmployeesObject = response.body();
                ArrayList listEmployees = (ArrayList) listEmployeesObject.get("listEmployees");
                Integer message = (Integer) listEmployeesObject.get("message");

                if (message == 1 ) {

                    final ArrayList<User> employees = new ArrayList<>();
                    lvUser = (ListView) view.findViewById(R.id.lvUser);

                    try {
                        Iterator itr = listEmployees.iterator();

                        while(itr.hasNext()) {
                            HashMap employee = (HashMap) itr.next();
                            employees.add(new User((String) employee.get("username"), (String) employee.get("fullname"), (String) employee.get("email"), (String) employee.get("type"), (Integer) employee.get("id"), (Integer) employee.get("status"), false));
                        }

                        final UserAdapter adapter = new UserAdapter(
                                getActivity(),
                                employees
                        );

                        lvUser.setAdapter(adapter);

                        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                User user = employees.get(position);

                                if (user.isActive()) {
                                    user.setActive(false);
                                    ids.remove(employees.get(position).getId());
                                }
                                else {
                                    user.setActive(true);
                                    ids.add(employees.get(position).getId());
                                }

                                employees.set(position, user);

                                adapter.updateRecords(employees);

                            }
                        });
                    }catch (Exception e){

                    }

                }
                else {
                    Toast.makeText(getContext(), "Get list users unsuccessful.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {

            }
        });

        btnSave = (Button) view.findViewById(R.id.btnSave);
//        img_accept = (ImageView) view.findViewById(R.id.img_accept);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.setUserIds(ids);
                NewAppointmentFragment newAppointmentFragment = new NewAppointmentFragment();
                showFragment(newAppointmentFragment);
            }
        });

        return view;
    }

    private void showFragment(Fragment newFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundleobj = new Bundle();
        Bundle bundle = this.getArguments();
        bundleobj.putCharSequence("customName" ,bundle.getString("customName"));
        bundleobj.putCharSequence("destination" ,bundle.getString("destination"));
        bundleobj.putCharSequence("startDate" ,bundle.getString("startDate"));
        newFragment.setArguments(bundleobj);
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}
