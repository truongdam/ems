package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

/**
 * Created by WINDOWS on 10/26/2017.
 */

public class AppointmentDetailFinishDTO {

    private String json_token;
    private Integer id;
    private String end_time;
    private Double input_cost;
    private String description;
    private String end_location;
    private String image_content;

    public AppointmentDetailFinishDTO(String json_token, Integer id, String end_time, Double input_cost, String description, String end_location, String image_content) {
        this.json_token = json_token;
        this.id = id;
        this.end_time = end_time;
        this.input_cost = input_cost;
        this.description = description;
        this.end_location = end_location;
        this.image_content = image_content;
    }

    public String getJson_token() {
        return json_token;
    }

    public void setJson_token(String json_token) {
        this.json_token = json_token;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Double getInput_cost() {
        return input_cost;
    }

    public void setInput_cost(Double input_cost) {
        this.input_cost = input_cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnd_location() {
        return end_location;
    }

    public void setEnd_location(String end_location) {
        this.end_location = end_location;
    }

    public String getImage_content() {
        return image_content;
    }

    public void setImage_content(String image_content) {
        this.image_content = image_content;
    }
}
