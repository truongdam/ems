package emsapp.dungvmnguyen.dhbk.emsapp.rest.dto;

public class User {

    private String username;
    private String fullname;
    private String email;
    private String type;
    private Integer id;
    private Integer status;
    private boolean active;

    public User(String username, String fullname, String email, String type, Integer id, Integer status, boolean active) {
        this.username = username;
        this.fullname = fullname;
        this.email = email;
        this.type = type;
        this.id = id;
        this.status = status;
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
